from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# Length of the ducts
L = 10 # [m]

# Width of the ducts
width = 0.1 # [m]

# gap between ducts
gap = 0.05 # [m]

# Length of the absorbing layer
L_al = 1 # [m]

current_y=0
for i in range(5) :
    duct_i = Rectangle(
        Point(0, current_y),
        Point(L, current_y + width) )
    if i == 0 :
        domain = duct_i
    else :
        domain = domain + duct_i
    current_y += width + gap

H.mesh = generate_mesh(domain, 500)

f_list = np.linspace(20, 1000, 15)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval(self, value, x) :
        value[0] = self.k_r
        if ( x[1] > 0 and
             x[1] < width and
             x[0] > L - L_al ) :
            value[1] = - 10
        elif ( x[1] > width + gap and
               x[1] < 2*width + gap and
               x[0] > L - L_al ) :
            value[1] = - 50 * (x[0] - (L - L_al))
        elif ( x[1] > 2*width + 2*gap and
               x[1] < 3*width + 2*gap and
               x[0] > L - L_al ) :
            value[1] = - 50 * (x[0] - (L - L_al))**2
        elif ( x[1] > 3*width + 3*gap and
               x[1] < 4*width + 3*gap and
               x[0] > L - L_al ) :
            value[1] = - 50 * (x[0] - (L - L_al))**3
        elif ( x[1] > 4*width + 4*gap and
               x[1] < 5*width + 4*gap and
               x[0] > L - L_al ) :
            value[1] = - 50 * (x[0] - (L - L_al))**4

    def value_shape(self):
        return (2,)

H.k_expression  = k_Expression(degree=0)

H.init()

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = DirichletBC(
        H.V,
        Constant((1.0, 0.0)),
        'on_boundary && near(x[0], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
