#  Convergence test for "Normal modes in simple axisymmetric domain" 

directory name: **convergence-tests/normal-modes-axisymmetric-simple** 


This code is an extenssion of [normal-modes-axisymmetric-simple-analytic-comparison](demo/normal-modes-axisymmetric-simple-analytic-comparison)

## Wavelength

The variable `wavelength` in this case is the wave length of a plane
wave propagating in the same media. However, because of the axial
symmetry, this is not precisely the distance between two consecutive
maxima of a propagating wave in the radial direction, computed
numerically, or obtained from the analytic solution. Nevertheless,
this variable `wavelength` is considered to be representative enough
of such length to be used to meassure convergence rates.

## Additional dependecies

pip
- pyyaml




## Screenshots
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/normal-modes-axisymmetric-simple/demo.py)
- [`screenshots.py`](demo/convergence-tests/normal-modes-axisymmetric-simple/screenshots.py)


[pvsm paraview file](demo/convergence-tests/normal-modes-axisymmetric-simple/results/vis.pvsm)



