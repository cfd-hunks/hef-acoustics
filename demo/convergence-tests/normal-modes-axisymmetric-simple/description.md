# Convergence test for "Normal modes in simple axisymmetric domain"

This code is an extenssion of [normal-modes-axisymmetric-simple-analytic-comparison](demo/normal-modes-axisymmetric-simple-analytic-comparison)

## Wavelength

The variable `wavelength` in this case is the wave length of a plane
wave propagating in the same media. However, because of the axial
symmetry, this is not precisely the distance between two consecutive
maxima of a propagating wave in the radial direction, computed
numerically, or obtained from the analytic solution. Nevertheless,
this variable `wavelength` is considered to be representative enough
of such length to be used to meassure convergence rates.

## Additional dependecies

pip
- pyyaml


