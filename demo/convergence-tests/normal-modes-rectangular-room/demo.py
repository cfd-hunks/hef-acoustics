from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys
import itertools
import pandas as pd
from pprint import pprint
from scipy import stats
import yaml

# import HEF-Acoustics main module
from hef import solver as H

width  = 1.1
height = 1.0

domain = Rectangle( Point(0.0,   0.0   ),
                    Point(width, height) )  # [m]

# This position prevents modes from being "fliped"
source_location = Point(0.01, 0.01)

def f_analytic_resonance(n1, n2) :
    """ Get frequency for mode (n1,n2) """
    # Pierce, A. D. Acoustics. Springer, 2019. Eq. (6.5.6)
    k = np.pi * ( (
        ( n1/width )**2 + ( n2/height )**2 )**0.5 )
    f = ( H.c*k ) / ( 2*np.pi )
    return f

class Analytic_solution(UserExpression) :
    n1 = None
    n2 = None
    # A normalization coefficient
    NC = 1
    def __init__(self, **kwargs) :
        super().__init__(**kwargs)
    def eval(self, value, x):
        # Pierce, A. D. Acoustics. Springer, 2019. Eq. (6.5.5)
        value[0] = self.NC*(
              np.cos(self.n1*np.pi*x[0]/width )
            * np.cos(self.n2*np.pi*x[1]/height) )
        value[1] = value[0]
    def value_shape(self):
        return (2,)

analytic_solution = Analytic_solution(degree = 5)

f_list = [340, 550]
# Get all the modes in the range defined with f_list
f_ar = dict()
n1   = 0
n2   = 0
i    = 0
while True :
    f = f_analytic_resonance(n1,n2)
    if f >= min(f_list) :
        if f <= max(f_list) :
            f_ar[i] = {
                "f"  : f,
                "n1" : n1,
                "n2" : n2 }
            n1 += 1
            i  += 1
        else :
            if n1 == 0 and n2 > 0 :
                break
            n1 = 0
            n2 += 1
    else :
        n1 += 1

test_data = {
    "i"                      : [],
    "n1"                     : [],
    "n2"                     : [],
    "f"                      : [],
    "error_L2"               : [],
    "error_percent"          : [],
    "N"                      : [],
    "wavelength"             : [],
    "cell_inradius_mean"     : [],
    "cell_inradius_std"      : [],
    "cell_circumradius_mean" : [],
    "cell_circumradius_std"  : [],
    "mesh_edge_mean"         : [],
    "mesh_edge_std"          : []}

i_prev = None

N_list = np.logspace(
    np.log10(2),
    np.log10(10),
    8)

for n, (i, N) in enumerate(itertools.product(f_ar, N_list)) :

    H.f = f_ar[i]["f"]

    if not i == i_prev :
        output_dir = f'./results/i_{i}'
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
        fid_p   = File(f'{output_dir}/p.pvd')
        fid_pa  = File(f'{output_dir}/pa.pvd')
        print(f"-"*80)

    # Recalculate the wavelength
    wavelength = H.c/H.f

    print(f"Working on run {n}:")
    print(f"    f={H.f:0.2f} Hz, i={i}")
    print(f"    n1={f_ar[i]['n1']}, n2={f_ar[i]['n2']}")
    print(f"    wavelength={wavelength:0.2f}, N={N:0.2f}")

    # It is easier to see: M/N = max(width,eight)/wavelength
    M = N*max(width,height)/wavelength

    H.mesh = generate_mesh(domain, M)

    edge_size_list = [
        edge.length()
        for edge
        in edges(H.mesh) ]
    cell_size_list = [
        (cell.inradius(), cell.circumradius())
        for cell
        in cells(H.mesh) ]

    mesh_size_parameters = {
        "cell_inradius_mean"    : float(np.mean([ x[0] for x in cell_size_list])),
        "cell_inradius_std"     : float(np.std( [ x[0] for x in cell_size_list])),
        "cell_circumradius_mean": float(np.mean([ x[1] for x in cell_size_list])),
        "cell_circumradius_std" : float(np.std( [ x[1] for x in cell_size_list])),
        "mesh_edge_mean"        : float(np.mean(edge_size_list)),
        "mesh_edge_std"         : float(np.std( edge_size_list)) }

    H.init()
    H.update_frequency()

    bc = PointSource(H.V, source_location, 1)
    A, b = H.Ab_assemble(bc)
    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    fid_p << (p_c, float(N))

    pa_c = Function(H.V, name="pa")
    analytic_solution.n1 = f_ar[i]["n1"]
    analytic_solution.n2 = f_ar[i]["n2"]
    analytic_solution.NC = 1
    assign(pa_c,  interpolate(analytic_solution, H.V))

    # Normalize the analytic solution
    p_r, p_i   = p_c.split()
    pa_r, pa_i = pa_c.split()
    I_numeric  = assemble( ((p_r**2  + p_i**2)**0.5)  * dx )
    I_analytic = assemble( ((pa_r**2 + pa_i**2)**0.5) * dx )
    analytic_solution.NC = I_numeric/I_analytic
    assign(pa_c,  interpolate(analytic_solution, H.V))
    fid_pa << (pa_c, float(N))

    # Calculate error
    error_L2 = errornorm(analytic_solution,p_c,"L2")
    ref = errornorm(
        analytic_solution,
        interpolate(Constant((0.0,0.0)), H.V),
        "L2")
    error_percent = 100*error_L2/ref

    test_data["i"].append(i)
    test_data["n1"].append(f_ar[i]["n1"])
    test_data["n2"].append(f_ar[i]["n2"])
    test_data["f"].append(H.f)
    test_data["error_L2"].append(error_L2)
    test_data["error_percent"].append(error_percent)
    test_data["N"].append(N)
    test_data["wavelength"].append(wavelength)
    test_data["cell_inradius_mean"].append(
        mesh_size_parameters["cell_inradius_mean"])
    test_data["cell_inradius_std"].append(
        mesh_size_parameters["cell_inradius_std"])
    test_data["cell_circumradius_mean"].append(
        mesh_size_parameters["cell_circumradius_mean"])
    test_data["cell_circumradius_std"].append(
        mesh_size_parameters["cell_circumradius_std"])
    test_data["mesh_edge_mean"].append(
        mesh_size_parameters["mesh_edge_mean"])
    test_data["mesh_edge_std"].append(
        mesh_size_parameters["mesh_edge_std"])
    
    i_prev = i

df_test_data = pd.DataFrame(test_data)

df_test_data.to_csv(f'./results/test_data.csv')

df = df_test_data.pivot_table(
    index="N",
    columns="i",
    values="error_percent")

df = df.rename(
    columns={
        key: f"(i={key}) (n1={item['n1']};n2={item['n2']})"
        for (key, item)
        in f_ar.items() } )

df.to_csv(
    f'./results/error_percent+N+i.csv')

error_percet_avg = [
    (key, np.mean(row))
    for (key, row)
    in df.iterrows() ]

slope, intercept,_,_,_ = stats.linregress(
    [ np.log10(item[0])
      for item
      in error_percet_avg ],
    [ np.log10(item[1])
      for item
      in error_percet_avg ] )
eta = -slope

tendency = {
    "N" : [ item[0] for item in error_percet_avg ],
    "Ep" : [
        (10**intercept)*(item[0]**slope)
        for item
        in error_percet_avg ] }

N1 = (10**(-intercept))**(1/slope)

df_tendency = pd.DataFrame(tendency)
df_tendency.to_csv(f'./results/tendency.csv')

with open(f'./results/parameters.yaml', 'w') as fd :
    yaml.dump({
        "convergence_order": float(slope),
        "linregress_intercept" : float(intercept) },
              fd,
              default_flow_style=False)

with open(f'./results/parameters.csv', 'w') as fd :
    fd.write("parameter,value\n")
    fd.write(f"eta, {eta:0.2f}\n")
    fd.write(f"N1, {N1:0.2f}\n")
    fd.write(f"linregress_intercept, {intercept:0.2f}\n")

with open(f'./results/N1.csv', 'w') as fd :
    Ep_min = min(tendency["Ep"])
    Ep_max = max(tendency["Ep"])
    fd.write("N,Ep\n")
    fd.write(f"{N1:0.2f},{Ep_min:0.2f}\n")
    fd.write(f"{N1:0.2f},{Ep_max:0.2f}\n")
