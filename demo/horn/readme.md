#  Horn 

directory name: **horn** 


- cylinder geometry
- axisymmetic
- resonance
- nomal modes
- frequency sweep
- absorbing region
- anechoic walls
- overriding `k_expression`
- wall velocity boundary condition
- radiation

## Additional dependencies

- shapely

## Screenshots
<img src="demo/horn/results/screenshot_1.png" height="120">
<img src="demo/horn/results/screenshot_2.png" height="120">
<img src="demo/horn/results/screenshot_3.png" height="120">
<img src="demo/horn/results/screenshot_4.png" height="120">
<img src="demo/horn/results/screenshot_5a.png" height="120">
<img src="demo/horn/results/screenshot_5b.png" height="120">
<img src="demo/horn/results/screenshot_6a.png" height="120">
<img src="demo/horn/results/screenshot_6b.png" height="120">


Scripts:
- [`demo.py`](demo/horn/demo.py)
- [`screenshots.py`](demo/horn/screenshots.py)


[pvsm paraview file](demo/horn/results/vis.pvsm)



