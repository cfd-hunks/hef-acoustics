from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on

# Medium variables
#---- air
H.c = 343    # [m/s]
#---- second gas
c2 = 268.6

# Incidence angle
theta = 40 # [degrees]

# Mass density of the second gas, not currently used (!!!)
rho_2 = 1.98

# Frequency
H.f = 40e3

# Domain
width  = 0.35
height = width

# Absorbing boundary
ab_width = 0.1
# Radius
ab_r1 = 0.10*1.1  # [m]
ab_r2 = min(width,height)/2
ab_min = 0
ab_max = 800

# Exciting transducer
#
# - Distance to the reflecting point
et_D       = 0.10 # [m]
# - Position of the center
et_x, et_y = (width/2 - et_D, height/2)
# - Width
et_width   = 0.04

# Receiving transducer
#
# - Distance to the reflecting point
rt_D     = et_D # [m]
# - Width
rt_width = 0.04
# - The rest of the parameters for the receiving transducer are
# - calculated within the main function.

mesh_refinement = 40

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')
fid_m = File(f"{output_dir}/materials.pvd")

# gas-gas interface geometry helper
class gg_interface :

    def __init__(self) :
        self.reset()

    def reset(self) :

        self.x1 = width/2
        self.y1 = height/2
        self.x2 = 3*width/4
        self.y2 = (
            self.y1
            + (self.x2-self.x1)*np.tan(np.pi*(90 - theta)/180) )

        self.slope = (self.y2-self.y1)/(self.x2-self.x1)

        # components of a normal unitary vector
        self.n_x = self.y2 - self.y1
        self.n_y = self.x1 - self.x2
        n_tmp = np.sqrt(self.n_x**2 + self.n_y**2)
        self.n_x = - (self.y2 - self.y1)/n_tmp
        self.n_y = - (self.x1 - self.x2)/n_tmp

    def get_y(self, x) :
        """return the "y" value, of the point over the interface corresponding to
        "x".
        """
        return self.y1 + (x-self.x1)*self.slope

    def get_x(self, y) :
        """return the "x" value, of the point over the interface corresponding to
        "y".
        """
        return self.x1 + (y-self.x1)/self.slope

    def distance_to_border(self, x, y) :
        D = (
            (   self.slope * (x - self.x1)
              - (y - self.y1) )
            /
            (   self.slope * self.n_x
              - self.n_y ) )
        return D

ggi = gg_interface()

def ab_set_ki(x, y) :
    """ Imaginary part of k, over the absorbing boundary.
    """

    # set a default value
    k_i = 0.0

    r = ((x-width/2)**2 + (y-height/2)**2)**0.5
    if r > ab_r1 :
        k_i = - (ab_min
                 + (  (ab_max - ab_min)
                      * ((r - ab_r1)/(ab_r2 - ab_r1))**2  ))

    return k_i

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, materials=None, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i
        self.materials = materials

    def eval_cell(self, value, x, cell):
        value[1] = ab_set_ki(x[0], x[1])
        if self.materials[cell.index] == 2 :
            value[0] = 2*pi*H.f/c2
        else :
            value[0] = 2*pi*H.f/H.c

    def value_shape(self):
        return (2,)

def main () :

    # Receiving transducer
    #
    # - Position of the center
    phi_aux_1 = 2*(90 - theta)
    rt_x, rt_y = (
        width/2  + rt_D*np.cos(np.pi*phi_aux_1/180),
        height/2 + rt_D*np.sin(np.pi*phi_aux_1/180) )
    # - Position of one end
    phi_aux_2 = 90 - 2*(90 - theta)
    rt_1_x, rt_1_y = (
        rt_x + (rt_width/2)*np.cos(np.pi*phi_aux_2/180),
        rt_y - (rt_width/2)*np.sin(np.pi*phi_aux_2/180) )
    # - Position of the other end
    rt_2_x, rt_2_y = (
        rt_x - (rt_width/2)*np.cos(np.pi*phi_aux_2/180),
        rt_y + (rt_width/2)*np.sin(np.pi*phi_aux_2/180) )

    # In case you changed theta, or something else
    ggi.reset()

    ##====> define domain and mesh

    domain = Rectangle(
        Point(0.0,  0.0),
        Point(width, height) )            # [m]

    et_d = Rectangle(
        Point(0.0 , et_y - et_width/2),
        Point(et_x, et_y + et_width/2) )
    R = et_x*1.2*0.25
    et_d += Circle(Point(et_x-R, et_y - et_width/2), R)
    et_d += Circle(Point(et_x-R, et_y + et_width/2), R)

    domain = domain - et_d

    # Subdomains
    first_gas = Polygon([
        Point(ggi.get_x(0.0), 0.0),
        Point(ggi.get_x(height), height),
        Point(0.0, height),
        Point(0.0, 0.0) ])                # [m]

    second_gas = Polygon([
        Point(width, 0.0),
        Point(width, height),
        Point(ggi.get_x(height), height),
        Point(ggi.get_x(0.0), 0.0) ])     # [m]

    # This is to remove regions outside the domain.
    first_gas  = domain - second_gas
    second_gas = domain - first_gas

    domain.set_subdomain(1, first_gas)
    domain.set_subdomain(2, second_gas)

    mesh = generate_mesh(domain, mesh_refinement)

    class Material_1(SubDomain):
        def inside(self, x, on_boundary):
            return x[1] >= ggi.get_y(x[0]) - DOLFIN_EPS

    class Material_2(SubDomain):
        def inside(self, x, on_boundary):
            return x[1] <= ggi.get_y(x[0]) + DOLFIN_EPS

    material_1 = Material_1()
    material_2 = Material_2()

    mesh = refine_mesh_on([
        lambda x : ( (x[0]-width/2)**2 + (x[1]-height/2)**2 )**0.5 < et_D*1.5,
        lambda x : ( (x[0]-width/2)**2 + (x[1]-height/2)**2 )**0.5 < et_D*1.1,
        lambda x : ( (x[0]-width/2)**2 + (x[1]-height/2)**2 )**0.5 < et_D*1.3 ],
                          mesh = mesh,
                          min_dx = 0.0001)

    materials = MeshFunction(
        "size_t",
        mesh,
        mesh.topology().dim(),
        0 )

    material_1.mark(materials, 1)
    material_2.mark(materials, 2)

    H.k_expression = k_Expression(
        materials=materials,
        degree=0 )

    materials.rename("materials", "materials")
    fid_m << (materials, float(theta))

    H.mesh = mesh

    ##====> Boundary conditions

    class Emitter(SubDomain):
        def inside(self, x, on_boundary) :
            return ( on_boundary and
                     x[0] <= et_x + DOLFIN_EPS and
                     x[0] >= et_x - DOLFIN_EPS and
                     x[1] < height - DOLFIN_EPS and
                     x[1] > 0 + DOLFIN_EPS )
    emitter = Emitter()
    P0_expression = Expression(
        ('exp(-0.5*pow((x[1] - mu)/s,N))', '0.0'),
        mu=height/2.0,
        s=et_width/2.0,
        N=2,
        degree=2 )

    # No boundary condition is otherwise specified, Neumann bc is implied.

    ##====> Initialize main module

    H.init()
    bc = [
        DirichletBC(
            H.V,
            P0_expression,
            emitter)]

    ##====> Compute solution

    A, b     = H.Ab_assemble(bc)
    p_c      = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()
    k_r, k_i = H.k_c.split()

    fid_p << (p_c,   float(theta))
    fid_k << (H.k_c, float(theta))

    ##====> Emulate a microphone (without a pre-defined domain)

    P_bar = 0 + 1j*0
    N = 100
    x_list, dx = np.linspace(
        start = rt_1_x, stop  = rt_2_x, num = N,
        retstep = True, endpoint = True)
    y_list, dy = np.linspace(
        start = rt_1_y, stop  = rt_2_y, num = N,
        retstep = True, endpoint = True)
    assert N==len(x_list), "inconsistent data"
    ## Note that "ds" is not used.
    # ds = np.sqrt(dx**2 + dy**2)
    N_effective = 0
    for x,y in zip(x_list, y_list) :
        pp = Point(x,y)
        try :
            P_bar       += p_r(pp) + 1j*p_i(pp)
            N_effective += 1
        except RuntimeError :
            # In case you are trying to meassure a point outside of the
            # domain.
            print(f"WARNING: point {x:0.2f}, {y:0.2f} for I out of bounds.")

    mic = np.abs(P_bar)/N_effective
    print(f"INFO: mic={mic:0.2g}")


if __name__ == '__main__':

    # Angle sweep
    for theta in np.arange(20,70+1,10) :

        print(f"Working on theta={theta:0.2f}°")

        main()
