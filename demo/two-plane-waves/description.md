# Propagation of two plane waves 

- 2D freespace geometry
- oblique propagation
- sinusoidal plane wave
- Dirichlet boundary condition
- analytic solution: sinusoidal plane wave
- angle sweep
- fixed frequency
- **no simulation**

## No simulation

There is no actual FEM simulation in this demo. The idea is only to
see the geometry of the interference (in magnitude and real part) of
two plane waves propagating with different angles.

## Geometry

One of the plane waves propagates with an angle $`\theta`$, and the
other with an angle $`-\theta`$.

In the case of $`\theta=0°`$ both plane waves move from left to right,
horizontally. In the case of $`\theta=90°`$, the first plane wave
moves from bottom to top, and the second plane wave moves from top to
bottom, vertically.

See that the case $`\theta=90°`$ has the minimum distance between red
horizontal lines in the figures for magnitude of the sum. These red
horizontal lines behave like solid reflective walls, they are
antinodes of the solution, which why there is a relation with the
results of [this other demo](demo/modes-rectangular-duct).


