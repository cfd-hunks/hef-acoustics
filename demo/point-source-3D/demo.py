from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
import hef

# For 3D problems is sometimes good idea to use `dolfin_adjoint`, if
# available you can change this line
hef.engine = "fenics"
# to
# hef.engine = "dolfin_adjoint"

from hef import solver as H
from hef.utils import refine_mesh_on

domain = Box(
    Point(-3.0, -3.0, -3.0),
    Point(3.0, 3.0, 3.0) )

source_location = Point(0.0, 0.0, 0.0)

mesh = generate_mesh(domain, 35)

mesh = refine_mesh_on([
    lambda x : ( x[0]**2 + x[1]**2 + x[2]**2)**0.5 < 1.5,
    lambda x : ( x[0]**2 + x[1]**2 + x[2]**2)**0.5 < 1,
    lambda x : ( x[0]**2 + x[1]**2 + x[2]**2)**0.5 < 0.5 ],
    mesh = mesh )

H.mesh = mesh

# If you have enough memory in your system, it is better to use
# `V_degree=2` here.
H.init(V_degree=1)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_pa = File(f'{output_dir}/pa.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 or abs(x[2]) > 1 :
            # This is k_i
            value[1] = - max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0,
                 abs(x[2]) - 1.0])
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

class Analytic_solution(UserExpression) :

    def __init__(self, k=0.0, **kwargs) :
        self.k = k
        super().__init__(**kwargs)

    def eval(self, value, x):
        r = (x[0]**2 + x[1]**2 + x[2]**2)**0.5

        # TODO: remove this 0.03, the automatic rescaling must take
        # care of this.
        value[0] = 0.03*(1/r)*cos(self.k*r)
        value[1] = 0.03*(1/r)*sin(self.k*r)
        
    def value_shape(self):
        return (2,)

H.k_expression = k_Expression(
    degree=0)

H.update_frequency(800)

analytic_solution = Analytic_solution()

bc = PointSource(H.V, source_location, 1)

A, b = H.Ab_assemble(bc)

p_c = Function(H.V, name="p")
solve(A, p_c.vector(), b)

# The use of f here is to be able to see frequency in
# paraview. It will appear as the time variable.
fid_p << (p_c,   float(H.f))
fid_k << (H.k_c, float(H.f))

# Define a subdomain to normalize analytic solution
sphere = AutoSubDomain(lambda x: (x[0]**2 + x[1]**2 + x[2]**2)**0.5 < 0.5)

sub_domains = MeshFunction(
    "size_t",
    H.mesh,
    H.mesh.topology().dim(),
    H.mesh.domains())

sub_domains.set_all(0)
sphere.mark(sub_domains, 1)
dxx = Measure('dx', domain=H.mesh, subdomain_data=sub_domains)

# Analytic solution
pa_c = Function(H.V, name="pa")
analytic_solution.k = H.k_expression.k_r
assign(pa_c,  interpolate(analytic_solution, H.V))

p_r, p_i   = p_c.split()
pa_r, pa_i = pa_c.split()

I_numeric  = assemble( ((p_r**2  + p_i**2)**0.5)  * dxx(1) )
I_analytic = assemble( ((pa_r**2 + pa_i**2)**0.5) * dxx(1) )

class Analytic_solution_normalized(UserExpression) :

    def __init__(self, analytic_solution, **kwargs) :
        self.analytic_solution = analytic_solution
        super().__init__(**kwargs)

    def eval(self, value, x) :
        value[0] = (I_numeric/I_analytic)*self.analytic_solution(x)[0]
        value[1] = (I_numeric/I_analytic)*self.analytic_solution(x)[1]
        
    def value_shape(self) :
        return (2,)

analytic_solution_normalized = Analytic_solution_normalized(analytic_solution)

assign(pa_c,  interpolate(analytic_solution, H.V))

fid_pa << (pa_c,   float(H.f))
