# Plane wave propagation

- 2D freespace geometry
- oblique propagation
- sinusoidal plane wave
- Dirichlet boundary condition
- analytic solution: sinusoidal plane wave
- frequency sweep