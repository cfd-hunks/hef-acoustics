#  Plane wave propagation 

directory name: **plane-wave** 


- 2D freespace geometry
- oblique propagation
- sinusoidal plane wave
- Dirichlet boundary condition
- analytic solution: sinusoidal plane wave
- frequency sweep

## Screenshots
<img src="demo/plane-wave/results/screenshot_1.png" height="120">
<img src="demo/plane-wave/results/screenshot_2.png" height="120">
<img src="demo/plane-wave/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/plane-wave/demo.py)
- [`screenshots.py`](demo/plane-wave/screenshots.py)


[pvsm paraview file](demo/plane-wave/results/vis.pvsm)



