from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys
from scipy import special

# import HEF-Acoustics main module
from hef import solver as H

# Forcing velocity amplitude
U0 = 6.0

cyl_R = 0.125

domain = (
    Rectangle(
        Point(-2.0, -2.0),
        Point(2.0, 2.0) )
    - Circle(
        Point(0.0, 0.0),
        cyl_R, 40) ) # [m]

H.mesh = generate_mesh(domain, 150)

f_list = np.arange(400, 2000, 100)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')
fid_U0 = File(f'{output_dir}/U0.pvd')
fid_pa  = File(f'{output_dir}/pa.pvd')

class Analytic_solution(UserExpression) :
    t = 0
    def __init__(self, t=0, **kwargs) :
        self.t = t
        super().__init__(**kwargs)

    def eval(self, value, x):
        r     = (x[0]**2 + x[1]**2)**0.5
        k     = H.k_expression.k_r
        omega = 2.0*np.pi*H.f
        # TODO: Check the sign here        
        complex_solution = - (
            (1j * H.rho * H.c * U0)
            * (  special.hankel2(0, r     * k)
               / special.hankel2(1, cyl_R * k) )
            * np.exp(1j * omega * self.t) )
        value[0] = complex_solution.real
        value[1] = complex_solution.imag

    def value_shape(self):
        return (2,)

analytic_solution = Analytic_solution(degree = 2)

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 :
            # This is k_i
            value[1] = - 10*max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0])**2
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

H.k_expression = k_Expression(
    degree=0)

# Pulsating cylinder
class Cyl_source(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 between(x[0], (-0.3, 0.3)) and
                 between(x[1], (-0.3, 0.3)) )

cyl_source = Cyl_source()

H.U0_expression = Constant((U0, 0.0))

H.init(boundary_U0=cyl_source)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b = H.Ab_assemble()

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))
    fid_k << (H.k_c, float(H.f))

    if H.f == f_list[0] :
        fid_U0 << H.U0_c

    pa_c = Function(H.V, name="pa")
    assign(pa_c,  interpolate(analytic_solution, H.V))
    fid_pa << (pa_c, float(H.f))
