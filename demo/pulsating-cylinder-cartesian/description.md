# Pulsating cylinder in free space

- 2D freespace geometry
- pulsating circle (cylinder)
- wall velocity boundary condition
- anechoic walls
- radiation
- frequency sweep
- analytic solution: (TODO) Check a sign (see code)

## Remarks

### Axial symmetry

This simulation correspond to a system with axial symmetry: a
pulsating cylindrical tube. However, it is *not* coded to use the
axisymmetric feature of the `hef` module. The symmetry axis is not in
the domain, but it is transversal to the domain, the tube is
represented with a a circle in a plane cartesian domain.