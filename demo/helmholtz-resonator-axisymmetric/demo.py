from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on

# Please note the resonator in this case is aligned with the x axis,
# while the other examples fo helmholtz resonator a aligned with the y
# axis. The reason for this is that for axisymetric systems the
# symmetry axis is the x axis.
H.axisymmetric = True

# Speed of sound
H.c = 343 # [m/s]

# Forcing velocity amplitude
U0 = 6.0

# Domain [m]
width  = 5
height = 5
resonator_width = 0.08

# If this syntax is confusing see "Uneven frequency lists" in
# doc/3_FEniCS_and_Python.md
frequency_list = sorted(set(
    list(np.arange(100, 600, 50))
    + list(np.arange(225, 275, 3) ) ))

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class Moving_wall(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 near(x[0], -resonator_width) )

moving_wall = Moving_wall()

H.U0_expression = Constant((U0, 0.0))

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        x1b = 5/4
        x0b = 5*(3/4)
        # value[1] is k_i
        if abs(x[0]) > x0b or abs(x[1]) > x1b :
            value[1] = - 5*max(
                [abs(x[0]) - x0b,
                 abs(x[1]) - x1b])**2
        else :
            value[1] = 0
    def value_shape(self):
        return (2,)

def theoretical_resonance_frequency (c,S,lp,V) :
    # Blackstock "fundamentals of physical acoustics" p. 154. Chap 4,
    # ec. (C-24)
    f = (c/(2*np.pi)) * (S/(lp*V))**0.5
    return f

with open(f"{output_dir}/analytic.csv",'w') as fd:
    # The use of a second column "ampl" here is just for paraview to
    # be able to draw this data in the figure.
    fd.write("freq,ampl\n")
    f_H = theoretical_resonance_frequency(
        c  = H.c,
        S  = np.pi*((resonator_width/10)**2),
        lp = 2*0.82*(resonator_width/10) + (resonator_width/2),
        V  = (np.pi*(resonator_width/2)**2)*(resonator_width/2) )
    fd.write(f"{f_H:0.2f},0.0001\n")
    fd.write(f"{f_H:0.2f},10\n")

domain = Rectangle(
    Point( -height, 0       ),
    Point(  height, width/2 ) ) # [m]

wall = Rectangle(
    Point( -height, 0       ),
    Point(  0     , width/2 ) ) # [m]

hole = (
    # Body
    Rectangle(
        Point( -resonator_width  , 0                ),
        Point( -resonator_width/2, resonator_width/2) )
    # Neck
    + Rectangle(
        Point( -resonator_width/2, 0                 ) ,
        Point(  0                , resonator_width/10) ) ) # [m]

domain = domain - ( wall - hole )

mesh = generate_mesh(domain, 120)

mesh = refine_mesh_on([
    lambda x : ( x[0]**2 + x[1]**2 )**0.5 < width/10,
    lambda x : ( x[0]**2 + x[1]**2 )**0.5 < width/20 ],
                      mesh = mesh )

H.k_expression = k_Expression(
    degree=0 )

H.mesh = mesh

H.init(boundary_U0=moving_wall)

for H.f in frequency_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b     = H.Ab_assemble()

    p_c      = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()
    k_r, k_i = H.k_c.split()

    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))


