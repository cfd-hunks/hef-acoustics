#  Set absorption value depending on frequency 

directory name: **absorption-depending-on-frequency** 


- 2D shoebox geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- k_i depending on frequency
- frequency sweep


## Screenshots
<img src="demo/absorption-depending-on-frequency/results/screenshot_1.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_2.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_3.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_4.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/absorption-depending-on-frequency/demo.py)
- [`screenshots.py`](demo/absorption-depending-on-frequency/screenshots.py)


[pvsm paraview file](demo/absorption-depending-on-frequency/results/vis.pvsm)



