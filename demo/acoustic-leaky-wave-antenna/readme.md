#  Acoustic Leaky Wave Antenna (ALWA) 

directory name: **acoustic-leaky-wave-antenna** 


- cylinder geometry
- axisymmetic
- absorbing region
- anechoic walls
- overriding `k_expression`
- wall velocity boundary condition
- radiation
- local mesh refinement
- multiple scales

## Published work using this code

> Omar Bustamante Palacios, Roberto Velasco-Segura, Eduardo Romero
> Vivas, "Estudio numérico de una antena acústica tipo guía de onda
> ranurada", SOMI XXXIV Congreso de Instrumentación, Morelia,
> Michoacán, México, del 16 al 18 de octubre de 2019. (Spanish)

> Omar Bustamante Palacios, Roberto Velasco Segura, Eduardo Romero
> Vivas, "Estudio numérico de antenas acústicas de onda con fuga", 8o
> Congreso Metropolitano de Modelado y Simulación Numérica, 5-7 de
> mayo 2021, CDMX, México. (Spanish)

> Omar Alejandro Bustamante Palacios, "Estudio numérico de antenas
> acústicas tipo guía de onda ranurada (Leaky Wave Antenna)", Masters
> dissertation, Posgrado en Ingeniería Eléctrica, 2021, ICAT-UNAM,
> México. (Spanish)

## Screenshots
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_1.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_2.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_3.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_4.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_5.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_6.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_7.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_8.png" height="120">


Scripts:
- [`demo.py`](demo/acoustic-leaky-wave-antenna/demo.py)
- [`screenshots.py`](demo/acoustic-leaky-wave-antenna/screenshots.py)


[pvsm paraview file](demo/acoustic-leaky-wave-antenna/results/vis.pvsm)



