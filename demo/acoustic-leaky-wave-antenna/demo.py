from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on

# Speed of sound
H.c = 343 # [m/s]

H.axisymmetric = True

# Forcing velocity amplitude
U0 = 6.0

# Domain [m]
width  = 4
height = 2

# Geometry of the LWA
duct_radius = 0.01
duct_slit_width  = 0.001
duct_wall_width  = 0.005
duct_slit_amount = 30
duct_cell_length = 0.01

frequency_list = [3500, 4000, 5000]

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_w = File(f'{output_dir}/wall.pvd')
fid_p = File(f'{output_dir}/p.pvd')
fid_k = File(f'{output_dir}/k.pvd')

class Moving_wall(SubDomain):
    def __init__(self, x_lid, **kwargs) :
        super().__init__(**kwargs)
        self.x_lid = x_lid
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 near(x[0], self.x_lid) and
                 x[1] <= duct_radius )

class k_Expression(UserExpression):

    def __init__(self, x1_min=0.0, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r    = k_r
        self.k_i    = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        x0b = width/2 - 1
        x1b = height - 1
        # value[1] is k_i
        if abs(x[0]) > x0b or abs(x[1]) > x1b :
            value[1] = - 10*max(
                [abs(x[0]) - x0b,
                 abs(x[1]) - x1b])**2
        else :
            value[1] = 0
    def value_shape(self):
        return (2,)

external_air = Rectangle(
    Point(-width/2, 0),
    Point( width/2, height) ) # [m]

duct_cell_region_length = (
    + (duct_slit_amount - 1) * (duct_slit_width + duct_cell_length)
    + duct_slit_width )

# Left vertical lid
duct_x_lid = -( 0.2 + (duct_cell_region_length/2) )
duct_wall = Rectangle(
    Point(
        duct_x_lid - duct_wall_width,
        0),
    Point(
        duct_x_lid,
        duct_radius + duct_wall_width) )

# Duct horizontall wall
duct_x0             = -(0.2 + (duct_cell_region_length/2) + duct_wall_width)
duct_section_length = 0.2
duct_wall += Rectangle(
    Point( duct_x0,                       duct_radius),
    Point( duct_x0 + duct_section_length, duct_radius + duct_wall_width) )
duct_x0 += duct_section_length 
for i in range(duct_slit_amount) :
    duct_x0 += duct_slit_width
    duct_section_length = duct_cell_length
    duct_wall += Rectangle(
        Point( duct_x0,                       duct_radius),
        Point( duct_x0 + duct_section_length, duct_radius + duct_wall_width) )
    duct_x0 += duct_section_length 
duct_wall += Rectangle(
        Point( duct_x0, duct_radius),
        Point( width/2, duct_radius + duct_wall_width) )

domain = external_air - duct_wall

H.mesh    = generate_mesh(domain, 300)
mesh_wall = generate_mesh(duct_wall, 200)

fid_w << mesh_wall

H.mesh = refine_mesh_on(
    lambda x : (
        x[0] >= duct_x_lid and
        x[1] <= duct_radius + duct_wall_width + 0.05 ),
                        mesh = H.mesh )

moving_wall = Moving_wall(duct_x_lid)

H.U0_expression = Constant((U0, 0.0))

H.k_expression = k_Expression(
    degree=0 )

H.init(boundary_U0=moving_wall)

for H.f in frequency_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b     = H.Ab_assemble()

    p_c      = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
