from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

H.axisymmetric = True

# Length of the duct with Helmholtz resonators
L = 1 # [m]

# Length of absorbing layer
L_a = 1 # [m]

# Radius of the duct
R = 0.01 # [m]

# Number of Helmholtz resonators
HR_n = 10

# Length of the neck of the Helmholtz resonators
HR_l = 0.002 # [m]

# Width of the neck of the Helmholtz resonators
HR_w = 0.002 # [m]

# Radius of the cavity of the Helmholtz resonators
HR_r = 0.012 # [m]

duct = Rectangle(
    Point(0, 0),
    Point(2*L + L_a, R) )

Delta_x   = L/(HR_n + 1)
current_x = Delta_x
for i in range(HR_n) :
    HR = (
        Rectangle(
            Point(current_x - HR_w/2, R),
            Point(current_x + HR_w/2, R + HR_l + HR_r)) +
        Circle(
            Point(current_x, R + HR_l + HR_r),
            HR_r,
            40 ))
    duct = duct + HR
    current_x += Delta_x

H.mesh = generate_mesh(duct, 1000)

f_list = np.linspace(200, 400, 24)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval(self, value, x) :
        value[0] = self.k_r
        if x[0] > 2*L :
            value[1] = - 15 * (x[0] - 2*L)**2
            
    def value_shape(self):
        return (2,)

H.k_expression  = k_Expression(degree=0)

H.init()

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = DirichletBC(
        H.V,
        Constant((1.0, 0.0)),
        'on_boundary && near(x[0], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
