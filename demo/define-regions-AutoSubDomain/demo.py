from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

domain = Rectangle( Point(0.0, 0.0),
                    Point(0.4, 1.0) ) # [m]

# Absorbing region
domain.set_subdomain(1,
                     Rectangle(
                         Point(0.0, 0.5),
                         Point(0.4, 1.0)))
absorbing_region = AutoSubDomain(
    lambda x:
    between(x[0], (0.0, 0.4)) and
    between(x[1], (0.5, 1.0))      )

H.mesh = generate_mesh(domain, 40)

materials = MeshFunction(
    "size_t",
    H.mesh,
    H.mesh.topology().dim(),
    0)
materials.rename("materials", "materials")

absorbing_region.mark(materials, 1)

H.init()

f_list = np.arange(800, 1600, 30)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')
fid_m  = File(f'{output_dir}/materials.pvd')

fid_m << materials

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, materials=None, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i
        self.materials = materials

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if self.materials[cell.index] == 0 :
            value[1] = 0.0
        elif self.materials[cell.index] == 1 :
            value[1] = - 10.0
        else :
            raise Exception(f"Unrecognized subdomain: {cell.index} {self.materials[cell.index]}")

    def value_shape(self):
        return (2,)

H.k_expression = k_Expression(
    materials=materials,
    degree=0)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    # Forcing on the side
    bc = DirichletBC(
        H.V,
        Constant((1.0, 0.0)),
        'on_boundary && near(x[0], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
