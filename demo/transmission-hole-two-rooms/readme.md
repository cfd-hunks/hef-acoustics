#  Transmission over an irregular hole between two rooms 

directory name: **transmission-hole-two-rooms** 


- 2D irregular geometry 
- wall velocity boundary condition
- transmission
- frequency sweep
- resonance
- normal modes

## Remarks

### Frequency range

See that the frequency range in this example is very small, with a
small frequency step. In is made this way to keep execution time low.

Moreover, in transmission studies data is not reported for isolated
frequencies, but for frequency bands. This means data presented here
would need an integration over frequency.

- issue #32

### Real and imaginary parts of presure

The real part of the pressure is always zero. This is a consequence of
the excitation having only a non-null value for the real part of the
velocity.

Thus, the only pressure fied important to plot is the imaginary part,
the amplitude is this same plot, just taking the "absolute value".

### Integration regions

To get average pressures in each room, an integration is performed in
specific regions, see screenshot 1. To define this region paraview uses
cell numbering, then if you modify the mesh, these numbers will change
and the region must be redefined. 

If you need to do this, see
[here](https://docs.paraview.org/en/latest/UsersGuide/selectingData.html#extracting-selections)
and use the buttons:
- "Select cells through (f)"
- "Copy active selection".




## Screenshots
<img src="demo/transmission-hole-two-rooms/results/screenshot_1.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_2a.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_2b.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_3.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/transmission-hole-two-rooms/demo.py)
- [`screenshots.py`](demo/transmission-hole-two-rooms/screenshots.py)


[pvsm paraview file](demo/transmission-hole-two-rooms/results/vis.pvsm)



