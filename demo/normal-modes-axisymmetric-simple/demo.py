from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

H.axisymmetric = True

# See that because of the axial symmetry this rectangle defines a
# "Petri Dish"
domain = Rectangle( Point(0.0, 0.0),
                    Point(0.1, 1.0) ) # [m]

H.mesh = generate_mesh(domain, 100)

H.c = 1

H.init()

f_list = np.arange(0.1, 2.0, 0.01)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    # See that because of the axial symmetry this point source is
    # acctually a ring.
    bc = PointSource(H.V, Point(0.01, 0.2), 1)

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))

