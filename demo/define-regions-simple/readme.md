#  Illustrate different options to define regions with specific acoustic properties 

directory name: **define-regions-simple** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: simple
- not so well defined materials interface

## Different options

There are a few different ways to "define regions", to be able to set
different physical properties. See the other demos for
`define-regions-`


## Screenshots
<img src="demo/define-regions-simple/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-simple/demo.py)
- [`screenshots.py`](demo/define-regions-simple/screenshots.py)


[pvsm paraview file](demo/define-regions-simple/results/vis.pvsm)



