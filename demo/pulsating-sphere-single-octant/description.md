# Pulsating sphere (single octant)

- **not working**
- 3D free space geometry
- single octant
- anechoic boundary condition
- fixed frequency
- pulsating sphere
- overriding `k_expression`
- local mesh refinement
- radiation

## Known issues

### The sphere is not smooth

Even when this script works better than `pulsating-sphere-simple`. In this
case since the surface of sphere is generated for the original mesh,
the refined sphere still have big flat surfaces.

The alternative seems to be to use `gmsh`, which have an option to de
define the mesh refinement for specific surfaces.
