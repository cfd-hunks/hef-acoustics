# Diffraction from a corner

- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency
