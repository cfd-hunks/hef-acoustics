# Helmholtz resonator (external excitation)

- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- Dirichlet boundary condition
- frequency sweep
- resonance
- radiation

## Known issues

### Two local maxima in frequency

This is probably related with the phase of the incident beam.