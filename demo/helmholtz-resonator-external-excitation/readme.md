#  Helmholtz resonator (external excitation) 

directory name: **helmholtz-resonator-external-excitation** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- Dirichlet boundary condition
- frequency sweep
- resonance
- radiation

## Known issues

### Two local maxima in frequency

This is probably related with the phase of the incident beam.

## Screenshots
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-external-excitation/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-external-excitation/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-external-excitation/results/vis.pvsm)



