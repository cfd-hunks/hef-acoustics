from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# Length of the ducts
L = 10 # [m]

# Width of the ducts
width = 0.1 # [m]

# gap between ducts
gap = 0.05 # [m]

# Length of the wedge
L_w = 0.2 # [m]

# Length of the absorbing layer
L_al = 1 # [m]

domain = (
    Rectangle(
        Point(0, 0),
        Point(L, width) ) +
    Rectangle(
        Point(0,   width + gap),
        Point(L, 2*width + gap) ) +
    Polygon([
        Point(0,     2*width + 2*gap),
        Point(L-L_w, 2*width + 2*gap),
        Point(L,     3*width + 2*gap),
        Point(0,     3*width + 2*gap) ]) +
    Polygon([
        Point(0,     3*width + 3*gap),
        Point(L-L_w, 3*width + 3*gap),
        Point(L,     4*width + 3*gap),
        Point(0,     4*width + 3*gap) ]) ) # [m]

H.mesh = generate_mesh(domain, 500)

patch_Y = 1/(H.rho * H.c)

f_list = np.linspace(20, 1000, 15)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')
fid_Y  = File(f'{output_dir}/Y.pvd')
fid_d  = File(f'{output_dir}/subdomains.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval(self, value, x) :
        value[0] = self.k_r
        if x[1] > 0 and x[1] < width and x[0] > L - L_al:
            value[1] = - 50 * (x[0] - (L - L_al))**2
        elif x[1] > 2*width + 2*gap and x[1] < 3*width + 2*gap and x[0] > L - L_al:
            value[1] = - 50 * (x[0] - (L - L_al))**2
            
    def value_shape(self):
        return (2,)

class Absorbing_patch(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 ((
                    x[1] >= width   + gap and
                    x[1] <= 2*width + gap and
                    near(x[0],L)) or
                  (
                    x[1] >= 3*width + 3*gap and
                    x[1] <= 4*width + 3*gap and
                    x[1] > 3*width + 3*gap + (width/L_w)*(x[0]-L+L_w) - 0.01 and
                    x[1] < 3*width + 3*gap + (width/L_w)*(x[0]-L+L_w) + 0.01 
                   )))

H.k_expression  = k_Expression(degree=0)
H.Y_expression  = Constant((patch_Y, 0))
absorbing_patch = Absorbing_patch()

H.init(boundary_Y = absorbing_patch)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = DirichletBC(
        H.V,
        Constant((1.0, 0.0)),
        'on_boundary && near(x[0], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))

    if H.f == f_list[0] :
        # Don't forget to pass `H.f` here, otherwise it will assign
        # zero, and this makes a little mess.
        fid_Y << (H.Y_c, float(H.f))
        fid_d << (H.subdomains, float(H.f))

    
