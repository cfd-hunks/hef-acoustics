# Point source in free space 2D

- 2D free space geometry
- anechoic boundary condition
- point source
- overriding `k_expression`
- frequency sweep
- radiation