#  Point source in free space 2D 

directory name: **point-source-2D** 


- 2D free space geometry
- anechoic boundary condition
- point source
- overriding `k_expression`
- frequency sweep
- radiation

## Screenshots
<img src="demo/point-source-2D/results/screenshot_1.png" height="120">
<img src="demo/point-source-2D/results/screenshot_2.png" height="120">
<img src="demo/point-source-2D/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/point-source-2D/demo.py)
- [`screenshots.py`](demo/point-source-2D/screenshots.py)


[pvsm paraview file](demo/point-source-2D/results/vis.pvsm)



