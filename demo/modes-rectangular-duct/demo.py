from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# The first item in this list corresponds to the plane wave mode. This
# list must contain at least this item. The following items are the
# amplitudes corresponding to the higher order modes.
mode_amplitude_list = [1, 1, 1]

# This duct will be excited with all of the modes set in the list
# `mode_amplitude_list`
domain = Rectangle(
    Point(0.0, 0.0),
    Point(1.0, 0.05) ) # [m]

# This create ducts to be excited with a single mode from the list
# `mode_amplitude_list`
gap = 0.01
duct_width = 0.05
y_current = gap + duct_width
for item in mode_amplitude_list :
    domain = domain + Rectangle(
        Point(0, y_current),
        Point(1, y_current + duct_width) )
    y_current += gap + duct_width

H.mesh = generate_mesh(domain, 200)

H.init()

f_list = np.arange(2e3, 8e3, 1e2)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval(self, value, x) :
        value[0] = self.k_r
        if x[0] > 0.5 :
            value[1] = - 150 * (x[0] - 0.5)**2

    def value_shape(self):
        return (2,)

class Excitation(UserExpression):

    def __init__(self, **kwargs) :
        super().__init__(**kwargs)

    def eval(self, value, x) :

        # First duct, use all the modes for the excitation
        y_current = 0
        if x[1] >= 0 and x[1] <= duct_width :
            value[0] = 0
            for i,item in enumerate(mode_amplitude_list) :
                value[0] += item * np.cos( i*np.pi*(x[1] - y_current)/duct_width )

        # Ducts excited with a single mode
        y_current = gap + duct_width
        for i,item in enumerate(mode_amplitude_list) :
            if x[1] >= y_current and x[1] <= y_current + duct_width :
                value[0] = item * np.cos( i*np.pi*(x[1] - y_current)/duct_width )
            y_current += gap + duct_width

        value[1] = 0

    def value_shape(self):
        return (2,)

excitation = Excitation()

H.k_expression = k_Expression(degree=0)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = DirichletBC(
        H.V,
        excitation,
        'on_boundary && near(x[0], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
