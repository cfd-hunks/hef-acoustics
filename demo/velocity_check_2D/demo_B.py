from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef import mic
from hef import velocity

# This is to have `p` and `u` in the same scale, between -1 and 1. In
# this way is easier to see the relative phases of real and imaginary
# parts, and the different components.
H.c   = 1
H.rho = 1

# Length of the ducts
L = 1 # [m]

# Width of the ducts
width = 0.1 # [m]

domain = Rectangle(
    Point(0, 0),
    Point(width, L) ) # [m]

# Mic positions
y_m_list = np.linspace(0, L, 100)
x_m_list = [ width/2 ] * len(y_m_list)

H.mesh = generate_mesh(domain, 100)

patch_Y = 1/(H.rho * H.c)

f_list = np.linspace(1, 20, 15)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/B_p.pvd')
fid_u   = File(f'{output_dir}/B_u.pvd')

class Absorbing_patch(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and near(x[1],L) )

H.Y_expression  = Constant((patch_Y, 0))
absorbing_patch = Absorbing_patch()

H.init(boundary_Y = absorbing_patch)

for i, H.f in enumerate(f_list) :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = DirichletBC(
        H.V,
        Constant((1.0, 0.0)),
        'on_boundary && near(x[1], 0.0)')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    fid_p << (p_c, i)

    p_r, p_i = p_c.split()

    mic.fields_to_measure["p"]["data"] = p_c
    u_c = velocity.calculate(p_r, p_i)
    fid_u << (u_c, i)
    mic.fields_to_measure["u"]["data"] = u_c

    mic.measure_fields(
        [ Point(x_m, y_m)
          for x_m, y_m
          in zip(
              x_m_list,
              y_m_list) ],
        filename = f"{output_dir}/B_mic_{i:04d}.csv")
    

    
