#  2D Check for the velocity field 

directory name: **velocity_check_2D** 


- 2D shoebox geometry
- wall admittance boundary condition
- reflecting walls
- Dirichlet boundary condition
- frequency sweep
- velocity field
- microphones

## Code

This demo is made in two scripts. One of them, "A", propagates a plane
wave within an "horizontal" rectangle; and the other, "B", in a
"vertical" rectangle.

The imaginary and real parts are plotted for the pressure field and
each of the components of the velocity field.

## Observations

For plane waves traveling in the positive direction of some spatial
axis the pressure and the velocity fields are in phase

```math
u = \frac{1}{\rho_0 c}p
```

And, since (see [here](doc/4_Paraview.md)):

> the real part of the pressure is found in paraview as `p_X`, and the
> imaginary part of pressure is `p_Y`.

Taking into account that, for this demo

```math
\begin{align}
c &= 1 \\
\rho &= 1
\end{align}
```

in these figures you can check the relations, for real and imaginary
parts, for each of the 2D components, described in the
[documentation](doc/details/velocity.pdf).





## Screenshots
<img src="demo/velocity_check_2D/results/screenshot_2.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_3.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_4.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_5.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_6.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_7.png" height="120">


Scripts:
- [`demo_A.py`](demo/velocity_check_2D/demo_A.py)
- [`demo_B.py`](demo/velocity_check_2D/demo_B.py)
- [`screenshots.py`](demo/velocity_check_2D/screenshots.py)


[pvsm paraview file](demo/velocity_check_2D/results/vis.pvsm)



