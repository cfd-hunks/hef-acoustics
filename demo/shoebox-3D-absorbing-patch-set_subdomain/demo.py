print(
"""
This example is currently not working, because `set_subdomain` is not
implemented in 3D. See https://fenicsproject.org/qa/5630/subdomains-in-3d/
""")

raise RuntimeError("Not yet implemented.")

from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
import hef
hef.engine = "dolfin_adjoint"
from hef import solver as H

# patch
patch_Y = 5.0 / ( H.rho * H.c )
patch_width  = 3.0   # [m]
patch_length = 2.0   # [m]
patch = Box(
    Point(0.5, 0.5, 0.0),
    Point(0.5 + patch_width, 0.5 + patch_length, 0.1) )  # [m]

domain = Box(
    Point(0.0, 0.0, 0.0),
    Point(4.1, 3.0, 2.0) )  # [m]

domain.set_subdomain(1, patch)

H.mesh = generate_mesh(domain, 40)

f_list = np.arange(120, 130, 0.5)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_Y  = File(f'{output_dir}/Y.pvd')
fid_d  = File(f'{output_dir}/subdomains.pvd')

H.Y_expression = Constant((patch_Y, 0.0))

H.init()

H.subdomains = MeshFunction("size_t",
                            H.mesh,
                            H.mesh.topology().dim() - 1,
                            H.mesh.domains())
H.dx = Measure('dx', domain=H.mesh, subdomain_data=H.subdomains)
H.ds = Measure('ds', domain=H.mesh, subdomain_data=H.subdomains)

nn = FacetNormal(H.mesh)

# If you use a complex value for Y, you are going to need some more algebra here.
print("Let I_1 be: \int_patch (dp_r/dn) ds / ( p_i omega rho Y \int_patch ds )")
print("Let I_2 be: \int_patch (dp_i/dn) ds / ( p_r omega rho Y \int_patch ds )")

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = PointSource(H.V, Point(1.0, 2.0, 1.0))

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()

    # The use of `H.f` here is to be able to see frequency in
    # paraview. It will appear as the time variable.
    fid_p << (p_c, float(H.f))

    if H.f == f_list[0] :
        fid_Y << H.Y_c
        fid_d << H.subdomains

    I_1 = assemble(
        ( dot(grad(p_r), nn)
          / (p_i*(patch_length*patch_width)*(2*np.pi*H.f)*H.rho*patch_Y) )
        * H.ds(1))

    I_2 = assemble(
        ( dot(grad(p_i), nn)
          / (p_r*(patch_length*patch_width)*(2*np.pi*H.f)*H.rho*patch_Y) )
        * H.ds(1))

    print(f"::: I_1 = {I_1:0.2f} approx 1")
    print(f"::: I_2 = {I_2:0.2f} approx -1")
