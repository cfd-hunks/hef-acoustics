#  Dipole in free space 2D 

directory name: **dipole** 


- 2D free space geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- radiation


## Screenshots
<img src="demo/dipole/results/screenshot_1.png" height="120">
<img src="demo/dipole/results/screenshot_2.png" height="120">
<img src="demo/dipole/results/screenshot_3.png" height="120">
<img src="demo/dipole/results/screenshot_4.png" height="120">
<img src="demo/dipole/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/dipole/demo.py)
- [`screenshots.py`](demo/dipole/screenshots.py)


[pvsm paraview file](demo/dipole/results/vis.pvsm)



