# Dipole in free space 2D

- 2D free space geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- radiation
