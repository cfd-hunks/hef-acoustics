from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on


domain = (
    Rectangle(
        Point(-3.0, -3.0),
        Point(3.0, 3.0) )
    -
    # emitter
    Rectangle(
        Point(-0.05, -0.005),
        Point( 0.05,  0.005)) )

mesh = generate_mesh(domain, 120)

mesh = refine_mesh_on(
        lambda x : ( x[0]**2 + x[1]**2 )**0.5  < 0.2 ,
        mesh = mesh )

H.mesh = mesh

H.init()

f_list = np.linspace(500, 1000, 12)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 :
            # This is k_i
            value[1] = - max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0])**2
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

class Emitter(SubDomain):
    def inside(self, x, on_boundary) :
        return ( on_boundary and
                 ( x[0]**2 + x[1]**2 )**0.5 < 0.5 )
emitter = Emitter()
P0_expression = Expression(
    ('(x[1]/abs(x[1]))', '0.0'),
    degree=2 )
    
H.k_expression = k_Expression(
    degree=0)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = [
        DirichletBC(
            H.V,
            P0_expression,
            emitter)]

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
