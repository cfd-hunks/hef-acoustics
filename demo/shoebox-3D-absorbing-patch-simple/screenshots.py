# This script is inteded to be run with pvbatch

from paraview.simple import *
import re

# load state
LoadState(
    './results/vis.pvsm',
    LoadStateDataFileOptions='Search files under specified directory',
    DataDirectory='./results')

for l_name, l_obj in GetLayouts().items() :
    SetActiveView(GetViewsInLayout(l_obj)[0])
    Render()
    l_obj.SetSize(1411, 821)
    s_search = re.search('Layout \#([0-9+])', l_name[0])
    if s_search :
        num = s_search.group(1)
        s_name = f"screenshot_{num}.png"
    else :
        s_name = f"{l_name[0]}.png"
    SaveScreenshot(
        f'./results/{s_name}',
        l_obj,
        SaveAllViews=1,    
        ImageResolution=[1411, 821])

