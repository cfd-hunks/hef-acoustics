from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
import hef

# For 3D problems is sometimes good idea to use `dolfin_adjoint`, if
# available you can change this line
hef.engine = "fenics"
# to
# hef.engine = "dolfin_adjoint"

from hef import solver as H

# patch
patch_Y = 5.0 / ( H.rho * H.c )
patch_width  = 3.0   # [m]
patch_length = 2.0   # [m]

domain = (
    Box(Point(0.0, 0.0, 0.0),
        Point(4.1, 3.0, 2.0)) ) # [m]

H.mesh = generate_mesh(domain, 50)

f_list = np.arange(120, 130, 0.5)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_Y  = File(f'{output_dir}/Y.pvd')
fid_d  = File(f'{output_dir}/subdomains.pvd')

class Absorbing_patch(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 between(x[0], (0.5, 0.5+patch_width)) and
                 between(x[1], (0.5, 0.5+patch_length)) and
                 near(x[2], 0.0))

absorbing_patch = Absorbing_patch()

cell_markers = MeshFunction("bool", H.mesh, H.mesh.topology().dim() - 1)
cell_markers.set_all(False)
absorbing_patch.mark(cell_markers, True)
H.mesh = refine(H.mesh, cell_markers)

H.Y_expression = Constant((patch_Y, 0.0))

# If you have enough memory in your system, it is better to use
# `V_degree=2` here.
H.init(
    boundary_Y = absorbing_patch,
    V_degree = 1)

nn = FacetNormal(H.mesh)

# If you use a complex value for Y, you are going to need some more algebra here.
print("Let I_1 be: \int_patch (dp_r/dn) ds / ( p_i omega rho Y \int_patch ds )")
print("Let I_2 be: \int_patch (dp_i/dn) ds / ( p_r omega rho Y \int_patch ds )")

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = PointSource(H.V, Point(1.0, 2.0, 1.0), 1)

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))

    if H.f == f_list[0] :
        # Don't forget to pass `H.f` here, otherwise it will assign
        # zero, and this makes a little mess.
        fid_Y << (H.Y_c, float(H.f))
        fid_d << (H.subdomains, float(H.f))

    I_1 = assemble(
        ( dot(grad(p_r), nn)
          / (p_i*(patch_length*patch_width)*(2*np.pi*H.f)*H.rho*patch_Y) )
        * H.ds(1))

    I_2 = assemble(
        ( dot(grad(p_i), nn)
          / (p_r*(patch_length*patch_width)*(2*np.pi*H.f)*H.rho*patch_Y) )
        * H.ds(1))

    print(f"::: I_1 = {I_1:0.2f} approx 1")
    print(f"::: I_2 = {I_2:0.2f} approx -1")
