# Pulsating sphere (using gmsh)

- 3D free space geometry
- single octant
- usign gmsh (mesh refinement)
- anechoic boundary condition
- fixed frequency
- radiation
- overriding `k_expression`
- analytic solution: (TODO: implement) pulsating sphere

## Additional dependencies

Only to regenerate the mesh file `xdmf`
- pip
  - h5py
  - gmsh
  - meshio
- apt
  - gmsh