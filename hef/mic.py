from dolfin import *
import numpy as np
import re

mpi_comm = MPI.comm_world
mpi_rank = mpi_comm.Get_rank()
mpi_size = mpi_comm.Get_size()

fields_to_measure = {
    "p" : {
        "columns" : None,
        "data"    : None },
    "u" : {
        "columns" : None,
        "data"    : None } }

def measure_or_nan(point,field) :
    try :
        return field(point)
    except Exception as e :
        return float("nan")

def set_ftm_columns(dim) :
    """Set operations to calculate values for columns, as functions in
 field_to_measure.
    """

    global fields_to_measure

    if fields_to_measure["p"]["columns"] is None :
        fields_to_measure["p"]["columns"] = [
            ("p_r", lambda p : p[0]),
            ("p_i", lambda p : p[1]),
            ("p_A", lambda p : abs(      p[0] + 1j*p[1])),
            ("p_p", lambda p : np.angle( p[0] + 1j*p[1]))]

    if fields_to_measure["u"]["columns"] is None :
        if dim == 2 :
            fields_to_measure["u"]["columns"] = [
                ("u_r_x", lambda u : u[0]),
                ("u_i_x", lambda u : u[2]),
                ("u_A_x", lambda u : abs(      u[0] + 1j*u[2])),
                ("u_p_x", lambda u : np.angle( u[0] + 1j*u[2])),
                ("u_r_y", lambda u : u[1]),
                ("u_i_y", lambda u : u[3]),
                ("u_A_y", lambda u : abs(      u[1] + 1j*u[3])),
                ("u_p_y", lambda u : np.angle( u[1] + 1j*u[3]))]

        elif dim == 3 :
            fields_to_measure["u"]["columns"] = [
                ("u_r_x", lambda u : u[0]),
                ("u_i_x", lambda u : u[3]),
                ("u_A_x", lambda u : abs(      u[0] + 1j*u[3])),
                ("u_p_x", lambda u : np.angle( u[0] + 1j*u[3])),
                ("u_r_y", lambda u : u[1]),
                ("u_i_y", lambda u : u[4]),
                ("u_A_y", lambda u : abs(      u[1] + 1j*u[4])),
                ("u_p_y", lambda u : np.angle( u[1] + 1j*u[4])),
                ("u_r_z", lambda u : u[2]),
                ("u_i_z", lambda u : u[5]),
                ("u_A_z", lambda u : abs(      u[2] + 1j*u[5])),
                ("u_p_z", lambda u : np.angle( u[2] + 1j*u[5]))]

        else :
            raise RuntimeError("dim must me 2 or 3")


def measure_fields(point_list, filename=None) :

    # Check if this is 3D
    if fields_to_measure["p"]["data"].geometric_dimension() == 2  :
        dim = 2
    elif fields_to_measure["p"]["data"].geometric_dimension() == 3  :
        dim = 3

    set_ftm_columns(dim)

    fields = {
        key : fields_to_measure[key]
        for key
        in fields_to_measure.keys()
        if fields_to_measure[key]["data"] is not None }

    point_list = np.atleast_1d(point_list)

    data_case = dict()

    comp_list = {
        "x" : lambda point : point.x(),
        "y" : lambda point : point.y() }

    if dim == 3 :
      comp_list["z"] = lambda point : point.z()

    for field_name in fields :
        for column_name, _ in fields[field_name]["columns"] :
            data_case[column_name] = []

    if filename is not None :
        if re.search('\.csv$',filename,re.IGNORECASE) is None :
            filename = filename + ".csv"
        if mpi_size > 1 :
            # follow the way files are numbered for different
            # processes in vtu files.
            se = re.search(r'(.*[^_0-9])_*([0-9]+)\.(csv)$',filename,re.IGNORECASE)
            if se is not None :
                filename = f"{se.group(1)}_p{mpi_rank:03d}_{se.group(2)}.{se.group(3)}"
            else :
                filename = f"{filename[:-4]}_p{mpi_rank:03d}.{filename[-3:]}"
        columns_str = ",".join(comp_list.keys())
        column_name_list = []
        for field_name in fields :
            for column_name, _ in fields[field_name]["columns"] :
                column_name_list.append(column_name)
        columns_str += ","
        columns_str += ",".join(column_name_list)
        with open(filename, 'w') as f :
            f.write(columns_str)
            f.write("\n")

    for point in point_list :
        for field_name in fields :
            field_split = fields[field_name]["data"].split()
            field_point = [
                measure_or_nan(point, item)
                for item
                in field_split ]
            for column_name, op in fields[field_name]["columns"] :
                data_case[column_name].append(op(field_point))

    if filename is not None :
        with open(filename, 'a') as f :
            for i, point in enumerate(point_list) :
                data_str = ",".join([ f"{comp_list[comp](point)}"
                    for comp
                    in comp_list.keys() ])
                for column_name in column_name_list :
                    data_str += f",{data_case[column_name][i]}"
                f.write(data_str)
                f.write("\n")

    return data_case
