Postprocessing and visualization of data for this repository is made
using [Paraview](https://www.paraview.org/).

When you load data to paraview, and choose specific ways to display
this data, it is possible to save this configuration in a `pvsm` file,
using the option `File > Save State...`. Then, later you can recover
and modify the visualization, as long as you have access to the
original data.

In this repository, some configuration has been made to display
important aspects of the data, which are different for each example,
including:
- mesh refinement
- amplitude differences (specially in resonance)
- radiation and interference patterns
- normal mode profiles
- absorptive regions

The configuration of this visualization has been saved for each case,
in a `pvsm` file inside the `results` directory, corresponding to each
demo. Some screenshots of this visualization has been taken, they are
in the same subdirectory and those screenshots are used and listed in
different parts of this documentation[^1]. However, to explore the
data it's much better to use Paraview and play with its different
visualization options.

Then, to use the `pvsm` files, you need to:
- Obtain the original data, by now the recommended way is to generate
  it yourself (see issue #35).
- Use `File > Load State...`.
- Select the corresponding `vis.pvsm` file.
- Use the option `Search files under specified directory`.
- Select the directory where the data is, which very likely is already
  writen.

After that, You should see something like this.

<img src="utils/screenshot.png" height="240">

# Time and frequency

Paraview is a software tipically used for Computational Fluid Dynamics
(CFD), where is tipical to have fields depending on time.
However, in the present repository we are solving the Helmholtz
equation, which results from the wave equation by a procedure where
the time variable is removed. This variable is removed assuming a
harmonic time dependence of the physical solution.
Moreover, the solution of the Helmholtz equation tipically depend on
frequency.
It would be unfortunate to stop using the great tools of paraview
because of this difference.

Then, in this repository, the "time variable of paraview" is used
tipically for frequency (and, some other variables, see for instance [this demo](demo/oblique-incidence) and [this demo](doc/diffraction-rounded-corner)).

This is done while writing data to de output files, like this
```python
    fid_p << (p_c, float(H.f))
```
where, `H.f` is the frequency value.

Unfortunatelly, while reading the data in paraview these values are
referenced as "time", for instance here

<img src="utils/time.png" height="60">

# Real and imaginary parts of the complex solution

Since, for the FEniCS implementation, the real and imaginary parts of
the solution are stored as components of a vector `p_c`. Then, these
information is read by paraview as vector components, as well. For the
examples in this repository, results for pressure are stored in a
vector variable named `p`, then, the real part of the pressure is
found in paraview as `p_X`, and the imaginary part of pressure is
`p_Y`.

# Issues

https://gitlab.com/cfd-pizca/hef-acoustics/-/issues?label_name%5B%5D=paraview

# Footnotes

[^1]: Some screenshots are taken "by hand" using the menu `File > Save
    Screenshot...`. However in many cases, the generation of this
    screenshots is automated in a "screenshots.py" script, which is
    intended to be run as `pvbatch screenshots.py`. Unfortunatelly,
    this command will not run from the distributed `HEF-Acoustics`
    images, it would increase too much the size of the images. Then,
    if you want to run this command, please do it from a system where
    paraview is installed.
