#+TITLE: A Helmholtz Equation Implementation for FEM, Based on FEniCS Project
#+AUTHOR: Roberto Velasco-Segura and Cristian U. Martínez-Lule \\
#+AUTHOR: \vspace{20pt}\small{Acoustics and Vibrations Group,} \\
#+AUTHOR: \small{The Institute for Applied Sciences and Technology, ICAT} \\
#+AUTHOR: \small{UNAM, México.} \\
#+AUTHOR: \vspace{20pt}\footnotesize{181st Meeting of the Acoustical Society of America, Seattle, Washington, November 30, 2021.}

#+OPTIONS: H:2 toc:nil num:t title:nil
#+LATEX_CLASS: beamer
# #+LaTeX_CLASS_OPTIONS: [handout, t, aspectratio=169]
#+LaTeX_CLASS_OPTIONS: [presentation, t, aspectratio=169]
#+LATEX_HEADER: \usepackage{pgfpages}
#+LATEX_HEADER: \usepackage{physics}
# #+LaTeX_HEADER: \usepackage{enumitem}
# #+LATEX_HEADER: \mode<handout>{\setbeameroption{show notes} \pgfpagesuselayout{2 on 1}[a4paper,border shrink=5mm]}

# Thanks to https://tex.stackexchange.com/questions/22346
#+BEAMER_HEADER: \defbeamertemplate*{title page}{customized}[1][]
#+BEAMER_HEADER: {
#+BEAMER_HEADER:   \usebeamerfont{title}\usebeamercolor[blue]{title}\inserttitle\par\vspace{30pt}
#+BEAMER_HEADER:   \usebeamerfont{subtitle}\usebeamercolor[black]{subtitle}\insertsubtitle\par
#+BEAMER_HEADER:   \bigskip
#+BEAMER_HEADER:   \usebeamerfont{author}\usebeamercolor[black]{author}\insertauthor\par
#+BEAMER_HEADER:   \usebeamerfont{institute}\usebeamercolor[black]{institute}\insertinstitute\par
#+BEAMER_HEADER: }

# #+BEAMER_HEADER: \titlegraphic{\includegraphics[width=2cm]{images/unam.png}} 
#+BEAMER_HEADER: \date{}
#+BEAMER_HEADER: \expandafter\def\expandafter\insertshorttitle\expandafter{%
#+BEAMER_HEADER:   \insertshorttitle\hfill%
#+BEAMER_HEADER:   \insertframenumber\,/\,\inserttotalframenumber}
#+BEAMER_HEADER: \usepackage{multimedia}
#+BEAMER_HEADER: \setbeamerfont{title}{size=\Huge}
#+BEAMER_HEADER: \setbeamertemplate{navigation symbols}{}
#+BEAMER_HEADER: \setbeameroption{show notes on second screen}
# #+BEAMER_HEADER: \setbeameroption{show only notes}
# #+EXPORT_FILE_NAME: notes.pdf

# Thanks to https://tex.stackexchange.com/questions/559109
#+BEAMER_HEADER: \setbeamercolor{note page}{bg=gray, fg=white}
#+BEAMER_HEADER: \setbeamertemplate{note page}{%
#+BEAMER_HEADER: \begin{note box}
#+BEAMER_HEADER: \addtolength{\parskip}{\baselineskip}
#+BEAMER_HEADER: \usebeamercolor[black]{note page}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate body}{fg=black}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate subbody}{fg=black}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate subsubbody}{fg=black}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate item}{fg=black}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate subitem}{fg=black}%
#+BEAMER_HEADER: \setbeamercolor{itemize/enumerate subsubitem}{fg=black}%
#+BEAMER_HEADER: % \hline \hline \hline
#+BEAMER_HEADER: \insertnote \vfill  \insertframenumber \vspace{10pt}
#+BEAMER_HEADER: \end{note box}}

#+BEAMER_THEME: Szeged
# #+BEAMER_THEME: Rochester
# #+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)

#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

# The whole domain
#+LaTeX_HEADER: \newcommand{\D}{\Omega}

# The boundary of the whole domain
#+LaTeX_HEADER: \newcommand{\B}{{\partial\Omega}}

# The fraction of the boundary having non null Y
#+LaTeX_HEADER: \newcommand{\BY}{{\partial\Omega_Y}}

# The fraction of the boundary having non null U
#+LaTeX_HEADER: \newcommand{\BU}{{\partial\Omega_U}}

# 12 slides

#+BEGIN_EXPORT latex
  \begin{frame}[noframenumbering,plain]
  \vspace{20pt}
  \titlepage
  \vfill
  \end{frame}
#+END_EXPORT

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Good morning.
    
    My name is Roberto Velasco-Segura.

    Thank you so much for the oportunity to be here.
    
    I'm going to talk about this project, which is a Helmholtz
    equation implementation for the Finite Element Method, based on
    FEniCS.

** DISCLAIMER

   Please note that this slides describe an old state of the code,
   corresponding to this URL:

   https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/ASA_meeting_fall21

   In the current state of the code, some of the examples are
   different, and some of the results of the tests are a little
   better.
    
** Code repository: HEF-Acoustics

   https://gitlab.com/cfd-pizca/hef-acoustics
   
*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.60
     :END:

   \vspace{15pt}
   
   Based on:
   - @@latex:\raisebox{-0.1in}{@@
     #+ATTR_LATEX: :width 0.4in :center nil
     [[file:images/fenics.png]]
     #+LaTeX:}
     FEniCS Project
     
   - @@latex:\raisebox{-0.1in}{@@
     #+ATTR_LATEX: :width 0.4in :center nil
     [[file:images/paraview.png]]
     #+LaTeX:}
     Paraview

   - @@latex:\raisebox{-0.1in}{@@
     #+ATTR_LATEX: :width 0.4in :center nil
     [[file:images/gitlab.png]]
     #+LaTeX:}
     Gitlab
   
*** Column 3                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.40
     :END:

**** Contents of this presentation :B_block:
     :PROPERTIES:
     :BEAMER_col: 1
     :BEAMER_env: block
     :END:
     
      \vspace{20pt}
      \tableofcontents
      
*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Most of the postprocessing and visualization is made in paraview, the
    code is hosted in gitlab, and (here) is the URL.

    There you can find the names of some of the people that have
    contributed to this project.
    
    The proposed name for this code is "HEF-Acoustics".

    In this presentation I'm going to describe a few details of the
    Finite Element Method, mainly the weak form and the boundary
    conditions.

    After that, I will present some numerical results to validate the code.

    After that, some examples:
    \vspace{-15pt}
    - On the one hand, examples for fundamental, simple, acoustic systems.
    - On the other hand, some more elaborated examples, which we have
      been using for original research.
    
    Finally, I will mention some conclusion remarks.
  
* Finite Element Method
  
** <<x>>                           

   \vspace{-45pt}

*** Helmholtz equation        :B_block:
     :PROPERTIES:
     :BEAMER_col: 0.45
     :BEAMER_env: block
     :END:
   
    \begin{align}
    \nabla^2 p + k^2 p = 0
    \end{align}
    where
    \begin{align}
    p &= p_r + i p_i \qquad \text{pressure} \\
    k &= k_r + i k_i \qquad \text{wave number}
    \end{align}

*** Boundary conditions       :B_block:
     :PROPERTIES:
     :BEAMER_col: 0.55
     :BEAMER_env: block
     :END:

   - Totally reflective (Neumann, natural)
     \vspace{-10pt}
     \begin{align}
     \pdv{p}{\hat{n}} = 0
     \end{align}

   - Dirichlet 
     \vspace{-10pt}
     \begin{align}
     p= P_0
     \end{align}
   
   - Non null admitance (Robin) 
     \vspace{-10pt}
     \begin{align}
     \label{eq:bc_admitance}
     \pdv{p}{\hat{n}} =-i \omega \rho_0 Y p
     \end{align}

   - Vibrating wall (Neumann) 
     \vspace{-10pt}
     \begin{align}
     \label{eq:bc_vib}
     \pdv{p}{\hat{n}} = -i \omega \rho_0  U_0 
     \end{align}
    
*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    (Here) we have the Helmholtz equation, which describes stationary
    states, including propagation.

    A first approach for numeric implementation of this equation is to
    take the pressure $p$ and the wave number $k$ to be real
    numbers. However, to describe many of the relevant features of the
    acoustic systems these variables need to be considered complex
    numbers. As you can see, (here). I will return to this point in the
    next slide.

    The types of boundary conditions considered in this code are the
    following:
    \vspace{-15pt}

    - A natural boundary condition for the Finite Element
      Method. Which corresponds to a totally reflective wall, because
      it is a null derivative of the pressure amplitude, with respect
      to a coordinate transversal to the boundary.

    - We can also set the value of the pressure for some region, which is a
      Dirichlet boundary condition.

    - As third option, we can have the mentioned derivative to be
      proportional to the field $p$ itself. With that we get a
      partially reflective wall, with a coefficient related to the
      admitance.

    - Finally, we can set a fixed value for this derivative, and have a
      situation corresponding to a vibrating wall.
    
    # Right now you cannot combine \eqref{eq:bc_admitance} and
    # \eqref{eq:bc_vib}, but we are fixing that.

** Weak form (real part)

   $a(p,v) = L(v)$ 
   
   \begin{align}
   a_r &=
    - \int_\D   \nabla v_r \cdot \nabla p_r              \ \dd v 
    - \int_\D   \nabla v_i \cdot \nabla p_i              \ \dd v \\
   &+ \int_\D   ((k_r^2 - k_i^2)p_r - 2 k_r k_i p_i)v_r  \ \dd v 
    + \int_\D   ((k_r^2 - k_i^2)p_i - 2 k_r k_i p_r)v_i  \ \dd v \\
   &+ \rho_0 c
      \bigg(
             \int_\BY  k_r v_r(Y_i p_r + Y_r p_i)        \ \dd s 
           - \int_\BY  k_r v_i(Y_r p_r - Y_i p_i)        \ \dd s 
      \bigg) \\
   L_r &= 
      \rho_0 c
      \bigg(
    - \int_\BU  k_r v_r U_{0,i}                          \ \dd s 
    + \int_\BU  k_r v_i U_{0,r}                          \ \dd s
      \bigg) 
   \end{align}

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    (Here) we have the real part of the weak form for the Helmholtz
    equation. See that the real and imaginary parts of the variables
    are operated explicitly. This is because FEniCS doesn't currently
    support complex numbers. Then, every complex variable is taken to
    be a vector, of two components, which are are operated as real
    scalar numbers.

    We obtain this weak form with a standard procedure for the Finite
    Element Method:
    - take the product with a test function,
    - integrate over the simulation domain,
    - make use of the Gauss theorem,
    - get some surface integrals, where we apply the boundary
      conditions, mentioned before.
    
** <<x>>                           

   \vspace{-35pt}
   
*** Axisymmetric case         :B_block:
     :PROPERTIES:
     :BEAMER_col: 0.30
     :BEAMER_env: block
     :END:

     Just express the integrals in cylindrical coordinates. 

*** Anechoic walls and PML    :B_block:
     :PROPERTIES:
     :BEAMER_col: 0.70
     :BEAMER_env: block
     :END:
     
   - PML is not yet implemented. 
   - Anechoic walls are obtained using the imaginary part of the wave
     number, with quadratic increase.

     \vspace{20pt}

     #+ATTR_LATEX: :width 0.85\textwidth :center
     [[file:images/pulsating-cylinder-cartesian-2.png]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    For the axisymmetric version of the code, we only express the
    differential operators, and the integrals in cylindrical
    coordinates, and remove derivatives with respect to the angular
    coordinate, assuming nothing depends on this coordinate.

    # In practice, only a product with the radial coordinate is needed
    # inside the integrals.

    ----------------

    To have non-reflective walls, a formal Perfectly Matched Layer
    implementation (PML) would be desirable, but currently it is not
    implemented. We are using, with good results, the imaginary part
    of the wave number, increasing it quadratically over a wide layer.
    As you can see, (here), and (here).
    
    We think this could be equivalent to a PML, but further check is
    still needed.
   
* Validation

** Comparisson between numeric results an analytic solutions

   \vspace{-20pt}
 
*** Percent error               :B_block:
      :PROPERTIES:
      :BEAMER_col: 0.40
      :BEAMER_env: block
      :END:
     \begin{align*}
     E_p =  100 \cdot \frac{ | p_N - p_A | }{ | p_A | }
     \end{align*}
     - $p_N$ : Numeric result
     - $p_A$ : Analytic solution
     - norm is $L^2$ 
 
*** Convergence rate            :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :BEAMER_col: 0.60
     :END:
    \begin{align*}
    E_p = K \cdot N^{-\eta}
    \end{align*}
    - $\eta$ : Convergence rate
    - $N$ : Cell diameters per wavelength
    - $N_1$ : $N$ for $E_p$ in the order of 1%
    - $K$ : Some constant

*** Back to one column                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:

    \vspace{5pt}

    @@latex:\fbox{\parbox{\textwidth}{@@
    \hspace{20pt} 4 convergence tests were performed, but only 2 are presented here.
    @@latex:}}@@


*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    To validate the code we simulated some systems where analytic
    solutions are available.

    Then, we can evaluate the difference between numeric and analytic
    results, take a L2 norm, and normalize to get a "percent error".

    As with other numeric methods, the percent error is expected to
    decay exponentially with mesh refinement.

    As a measure of the refinement we use the "amount of cell
    diameters in a wavelength" $N$.
    
    Then, we measure:
    - the exponent $\eta$ of this decay, and
    - the value $N_1$, for which the percent error is in the order of
      1%.

    4 convergence tests were performed, but only 2 are presented
    here. The other two, and some further details can be found in the
    repository.
   
** Oblique plane wave propagation

   \vspace{-20pt}
   
*** Pressure field            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.25
    :END:
   
    [[file:images/E-imgp_2.pdf]]

    - $\eta = 1.99$
    - $N_1 \approx 60$

*** Convergence rate          :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.80
    :END:
   
    [[file:images/E-imgp_1.pdf]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Here we see results from a set of simulations for plane waves, at
    different angles.

    We use a Dirichlet boundary condition, equal to the analytic
    solution, over the whole boundary.

    The different lines (here) correspond to the refinement of the
    simulations for different angles.
    
    As the mesh refines we see, as expected, a reduction on the
    "Percent error", to the point that we have 1% error for $N$ close
    to 60.

    We see a straight gray line corresponding to $N$ to the power
    minus 1.99, which we think describes the global tendency of the
    data.

    The value 1.99 is obtained taking averages and using a linear
    regression over the logarithms of the data.
    
    Then, we say the convergence rate $\eta$ is 1.99, almost 2, which
    would correspond to a formal second order method.

    
** Pulsating tube             :noexport:

     - $\eta = 1.65$
     - $N_1 \approx 30$
   
** Normal modes in shoebox    :noexport:

*** Pressure field            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.25
    :END:
   
    [[file:images/B-imgp_2.pdf]]

    - $\eta = 1.7$
    - $N_1 = 80$
    
*** Convergence rate          :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.80
    :END:
   
    [[file:images/B-imgp_1.pdf]]
   
** Normal modes in simple axisymmetric enclosure

   \vspace{-20pt}
   
*** Pressure field            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.25
    :END:

     #+BEGIN_center
     #+ATTR_LATEX: :width 0.3\textwidth :center
     [[file:images/C-imgp_2.pdf]]
     #+ATTR_LATEX: :width 0.4\textwidth :center
     [[file:images/normal-modes-axisymmetric-simple-analytic-comparison-1.pdf]]
     #+END_center
     
     - $\eta = 1.65$
     - $N_1 \approx 25$
     
*** Convergence rate          :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.80
    :END:
   
    [[file:images/C-imgp_1.pdf]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Here is another convergence test for a similar case. This is a simple
    axisymmetric enclosure where we know normal modes correspond to
    Bessel functions.

    The geometry is similar to a Petri dish.

    The different lines correspond to the refinement of simulations
    for the different normal modes.

    We see convergence parameters a little different from the previous
    case:
    - only 25 cell diameters per wavelength are needed, to get an
      error of around 1%, and
    - the convergence rate is a little lower: 1.65.

    Again, the gray thick line correspond to N to the power minus 1.65.

* Examples
  
** Examples: typical acoustics systems

#   \vspace{-50pt}
   
*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.50
     :END:

     #+LaTeX:\raisebox{0.3in}{
     \parbox{0.8in}{Diffraction from a \\ corner}
     #+LaTeX:}
     #+ATTR_LATEX: :width 1.8in :center
     [[file:images/difraction-corner-1.pdf]]

     \vspace{20pt}
     
     #+LaTeX:\raisebox{0.5in}{
     \parbox{1.5in}{Normal modes in a rectangular room}
     #+LaTeX:}
     #+ATTR_LATEX: :width 1.1in :center
     [[file:images/normal-modes-rectangular-room-analytic-comparison-1.pdf]]
     
*** Column 2                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.50
     :END:

     #+LaTeX:\raisebox{0.3in}{
     \parbox{0.8in}{Pulsating cylinder}
     #+LaTeX:}
     #+ATTR_LATEX: :width 1.8in :center
     [[file:images/pulsating-cylinder-cartesian-1.pdf]]

     \vspace{20pt}

     #+LaTeX:\raisebox{0.5in}{
     \parbox{0.8in}{Pulsating sphere (3D)}
     #+LaTeX:}
     #+ATTR_LATEX: :height 1.2in :center
     [[file:images/pulsating-sphere-gmsh-1.pdf]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    \vspace{-10pt}

    Having an idea of the magnitude of the error depending on the
    mesh refinement. We apply the code to other acoustic systems.
    
    There are currently 25 examples in the repository, some of which
    are very similar to each other, but have a little change to make
    emphasis on the impact of some detail.

    Here we see:
    - A Gaussian beam coming from the left, parallel to a surface,
      encountering a corner, and diffracting.
    - Below, normal modes in a rectangular enclosure.
    - On the top right, a circle with a vibrating boundary condition,
      in the center of a plane, and anechoic layers over the rest of
      the walls. This system correspond to a pulsating cylinder.
    - And last, a pulsating sphere, very similar to the previous case,
      but on full 3D.
  
   # - transmission through an irrgular hole
    
** Helmholtz resonator

*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.20
     :END:
   
     The bottom wall of the cavity is vibrating, with constant
     amplitude.

*** Column 2                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.80
     :END:

     \vspace{-20pt}
   
     #+ATTR_LATEX: :width \textwidth
     [[file:images/helmholtz-resonator-vibrating-wall-1.png]]

*** <<x>>                       :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Another example of a typical acoustics system is the Helmholtz
    resonator.

    In this case, there is a vibrating wall at the bottom of the
    enclosure.

    We see the expected increase for amplitude at the resonance
    frequency.

    We see, also, the radiation has much lower pressure amplitude than the
    one in the enclosure.

    Finally, we can see the velocity, which is related to the gradient
    of the pressure, is much higher at the neck. And, there are small
    regions (here, and here) corresponding to the end corrections for
    the neck length.
     
** Oblique incidence, reflection and refraction

*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.20
     :END:
   
   Two media having different speed of sound.

   \vspace{10pt}
   
   Ortega-Aguilar, Alejandro, et al. Proceedings of Meetings on
   Acoustics 179ASA, 2020.

*** Column 2                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.80
     :END:

     \vspace{-20pt}
     
   #+ATTR_LATEX: :width \textwidth 
   [[file:images/oblique-incidence-1.png]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    In this example we have a Gaussian beam. We are interested in
    preventing eventual reflections in the left boundary, that's why
    we have this special geometry.

    At his point, the beam encounters a second media, with a different speed
    of sound. Then, a reflection, and a refraction beam are
    generated. Both finally dissipate inside anechoic layers.

    This is a two-dimensional simulation. However, we are using a
    similar 3D simulation to study the possibility of measuring speed
    of sound from a experimental setup like this.

    An important limitation of the code, for this case, is that
    different media cannot have different densities. It is interesting
    how density is not in the Helmholtz equation, but it is an
    important aspect of the solution of this case. Then, it should be
    a kind of boundary condition, which we think in this case leads to
    a Discontinuous Galerkin Method (DGM), which FEniCS can handle,
    and we are working on it.
   
** Horn (axisymmetric)

*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.20
     :END:
   
   - Resonance frequencies can be sharply recognized

   - Directional patterns depending on frequency are also observed
   
*** Column 2                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.80
     :END:

     \vspace{-20pt}
     
     #+ATTR_LATEX: :width \textwidth 
     [[file:images/horn-1.png]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    This is an axisymmetric example consisting of a long duct,
    excited in one end and having a horn in the other end.

    In this case, we can see two important aspects, 
    - the effective duct length, which we calculate from the resonance
      frequencies, and
    - the directional radiation patterns, depending also on
      frequency.

    We are currently studying, the role of the horn geometry over
    these two points.
     
** Leaky Wave Antenna (axisymmetric)

*** Column 1                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.20
     :END:

     O Bustamante, et al., 179th Meeting of the Acoustical Society of
     America, 2020.
     
     \vspace{10pt}
     
     Including membranes in the simulation would be good, but it is
     not yet ready (DGM).
   
*** Column 2                  :BMCOL:   
     :PROPERTIES:
     :BEAMER_col: 0.80
     :END:

     \vspace{-20pt}
     
     #+ATTR_LATEX: :width \textwidth 
     [[file:images/acoustic-leaky-wave-antenna-1.png]]

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Finally, this system is a cylindrical duct with slits, in such a
    manner that the interference of the radiation corresponding to
    individual slits creates a total directional radiation.

    This a Masters dissertation completed this year, presented in this
    meeting last year, as well. We are currently working on optimizing
    the geometry.

    This is another illustration on how the implementation of a
    Discontinuous Galerkin Method would benefit this code. In this
    case it would allow jumps in the numeric solution, which can model
    the presence of membranes inside the duct, as the ones used in
    experimental devices.
     
* Conclusions

** Conclusions

   - The implemented weak form contemplates a variety of useful
     situations.
     - Propagation, diffraction, absorption, resonance, reflection,
       etc.
          
   - Four convergence tests were performed.
     - The convergence rate found is between 1.65 an 2 (second order).
     - Validated (cartesian, and axisymmetric).

   - When simulating for a real application. How many cell diameters
     are needed per wavelegth to get an error around 1%?
     - It depends on the specifics of the system, from 25 to 80.

   - Code
     - Code is available for: solver, examples and convergence tests.
     - Many features come from the FEniCS and Paraview base code.
     - Discussion is open in the issues section of the repository.

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    \vspace{-15pt}
    Finally, here are some conclusion remarks.
    \vspace{-15pt}
    - (first) The implemented weak form contemplates a variety of useful
      situations.
      - Propagation, diffraction, absorption, resonance, reflection,
        and some other.
           
    - (second) Four convergence tests were performed.
      - The convergence rate found is between 1.65 an 2, which is close
        to results that could be found in a second order method.
      - We consider this code to be validated, for the cartesian, and
        axisymmetric cases.
 
    - (third) When simulating for a real application. How many cell diameters
      are needed per wavelegth to get an error around 1%?
      - It depends on the specifics of the system, from 25 to 80. We
        know this can be improved, and we are working on it.
 
    - (fourth) 
      - The code is available for: solver, examples and convergence tests.
      - Many features come from the FEniCS and Paraview base code. The
        engine of the numeric method is from FEniCS, and the
        visualization is from paraview.
      - Finally, we want to say that the discussion, over any aspect of
        this code, is open in the "issues" section of the repository.
    
   # even test A have $N_1$ close to 60

* Appendix
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
   
** <<x>>

   \vspace{-35pt}
   
*** @@latex:\Huge{Thanks!}@@  :B_block:
     :PROPERTIES:
     :BEAMER_col: 1
     :BEAMER_env: block
     :END:

     \vspace{15pt}
     
     @@latex:\parbox{\textwidth}{. \hspace{40pt} \Large{HEF-Acoustics} \\ . \hspace{40pt} \url{https://gitlab.com/cfd-pizca/hef-acoustics}}@@
     
     \vspace{15pt}
     
     #+BEGIN_center
     #+ATTR_LATEX: :width 1in :center nil
     [[file:images/unam.png]]
     \hspace{20pt}
     #+ATTR_LATEX: :width 0.8in :center nil
     [[file:images/icat.png]]
     \hspace{20pt}
     #+ATTR_LATEX: :width 2in :center nil
     [[file:images/asa.png]]
     #+END_center

     \vspace{4pt}
     
     @@latex:\footnotesize{181st Meeting of the Acoustical Society of America, Seattle, Washington, November 30, 2021.}@@

*** <<x>>                     :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:

    Thank you very much.
   
# Local Variables:
# ispell-dictionary: "en"
# End:

#  LocalWords:  compatibilities PML backend Paraview
