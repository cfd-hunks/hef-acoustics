

# Demonstration files

In this file you will find a complete list (automatically generated)
of the available demos, some of them in multiple versions, sorted
alphabetically (by directory name).

In addition, the following files have some discussion on these same
demonstrations from specific perspectives.



- [1 Acoustics](doc/1_Acoustics.md)
- [2 Finite Element Method](doc/2_Finite_Element_Method.md)
- [3 FEniCS and Python](doc/3_FEniCS_and_Python.md)
- [4 Paraview](doc/4_Paraview.md)
- [5 Convergence](doc/5_Convergence.md)


---

# Demo list

---

#  Set absorption value depending on frequency 


directory name: **absorption-depending-on-frequency** 


- 2D shoebox geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- k_i depending on frequency
- frequency sweep


<img src="demo/absorption-depending-on-frequency/results/screenshot_1.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_2.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_3.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_4.png" height="120">
<img src="demo/absorption-depending-on-frequency/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/absorption-depending-on-frequency/demo.py)
- [`screenshots.py`](demo/absorption-depending-on-frequency/screenshots.py)


[pvsm paraview file](demo/absorption-depending-on-frequency/results/vis.pvsm)



#  Comparison of different options for an absorbing layer 


directory name: **absorption-layer-comparison** 


- 2D shoebox geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep



**[See more details](demo/absorption-layer-comparison).**

<img src="demo/absorption-layer-comparison/results/screenshot_1.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_2.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_3.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_4.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/absorption-layer-comparison/demo.py)
- [`screenshots.py`](demo/absorption-layer-comparison/screenshots.py)


[pvsm paraview file](demo/absorption-layer-comparison/results/vis.pvsm)



#  Comparison of an absorbing layer with a boundary condition for the admittance 


directory name: **absorption-wall-and-layer** 


- 2D shoebox geometry
- absorbing layer
- wall admittance boundary condition
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep



**[See more details](demo/absorption-wall-and-layer).**

<img src="demo/absorption-wall-and-layer/results/screenshot_1.png" height="120">
<img src="demo/absorption-wall-and-layer/results/screenshot_2.png" height="120">
<img src="demo/absorption-wall-and-layer/results/screenshot_3.png" height="120">
<img src="demo/absorption-wall-and-layer/results/screenshot_4.png" height="120">
<img src="demo/absorption-wall-and-layer/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/absorption-wall-and-layer/demo.py)
- [`screenshots.py`](demo/absorption-wall-and-layer/screenshots.py)


[pvsm paraview file](demo/absorption-wall-and-layer/results/vis.pvsm)



#  Acoustic Leaky Wave Antenna (ALWA) 


directory name: **acoustic-leaky-wave-antenna** 


- cylinder geometry
- axisymmetic
- absorbing region
- anechoic walls
- overriding `k_expression`
- wall velocity boundary condition
- radiation
- local mesh refinement
- multiple scales



**[See more details](demo/acoustic-leaky-wave-antenna).**

<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_1.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_2.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_3.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_4.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_5.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_6.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_7.png" height="120">
<img src="demo/acoustic-leaky-wave-antenna/results/screenshot_8.png" height="120">


Scripts:
- [`demo.py`](demo/acoustic-leaky-wave-antenna/demo.py)
- [`screenshots.py`](demo/acoustic-leaky-wave-antenna/screenshots.py)


[pvsm paraview file](demo/acoustic-leaky-wave-antenna/results/vis.pvsm)



#  Beam propagation 3D 


directory name: **beam-propagation-3D** 


- **Not working**



**[See more details](demo/beam-propagation-3D).**



Scripts:
- [`demo.py`](demo/beam-propagation-3D/demo.py)




#  Illustrate boundary condition for velocity 


directory name: **boundary-condition-for-velocity** 


- 2D shoebox geometry
- pulsating circle (cylinder)
- wall velocity boundary condition
- frequency sweep
- reflecting walls
- resonance
- normal modes



**[See more details](demo/boundary-condition-for-velocity).**

<img src="demo/boundary-condition-for-velocity/results/screenshot_1.png" height="120">
<img src="demo/boundary-condition-for-velocity/results/screenshot_2.png" height="120">
<img src="demo/boundary-condition-for-velocity/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/boundary-condition-for-velocity/demo.py)
- [`screenshots.py`](demo/boundary-condition-for-velocity/screenshots.py)


[pvsm paraview file](demo/boundary-condition-for-velocity/results/vis.pvsm)



#  Convergence test for "Normal modes in simple axisymmetric domain" 


directory name: **convergence-tests/normal-modes-axisymmetric-simple** 


This code is an extenssion of [normal-modes-axisymmetric-simple-analytic-comparison](demo/normal-modes-axisymmetric-simple-analytic-comparison)



**[See more details](demo/convergence-tests/normal-modes-axisymmetric-simple).**

<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/normal-modes-axisymmetric-simple/demo.py)
- [`screenshots.py`](demo/convergence-tests/normal-modes-axisymmetric-simple/screenshots.py)


[pvsm paraview file](demo/convergence-tests/normal-modes-axisymmetric-simple/results/vis.pvsm)



#  Convergence test for "Normal modes in 2D rectangular domain" 


directory name: **convergence-tests/normal-modes-rectangular-room** 


This code is an extenssion of [normal-modes-rectangular-room-analytic-comparison](demo/normal-modes-rectangular-room-analytic-comparison)



**[See more details](demo/convergence-tests/normal-modes-rectangular-room).**

<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/normal-modes-rectangular-room/demo.py)
- [`screenshots.py`](demo/convergence-tests/normal-modes-rectangular-room/screenshots.py)


[pvsm paraview file](demo/convergence-tests/normal-modes-rectangular-room/results/vis.pvsm)



#  Convergence test for "plane wave propagation" 


directory name: **convergence-tests/plane-wave** 


This code is an extenssion of [plane-wave](demo/plane-wave).



**[See more details](demo/convergence-tests/plane-wave).**

<img src="demo/convergence-tests/plane-wave/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/plane-wave/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/plane-wave/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/plane-wave/demo.py)
- [`screenshots.py`](demo/convergence-tests/plane-wave/screenshots.py)


[pvsm paraview file](demo/convergence-tests/plane-wave/results/vis.pvsm)



#  Convergence test for "plane wave propagation" with a fixed mesh 


directory name: **convergence-tests/plane-wave-fixed-mesh** 


This code is an extenssion of [plane-wave](demo/plane-wave).



**[See more details](demo/convergence-tests/plane-wave-fixed-mesh).**

<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/plane-wave-fixed-mesh/demo.py)
- [`screenshots.py`](demo/convergence-tests/plane-wave-fixed-mesh/screenshots.py)


[pvsm paraview file](demo/convergence-tests/plane-wave-fixed-mesh/results/vis.pvsm)



#  Pulsating tube in axysimmetric simulation domain 


directory name: **convergence-tests/pulsating-cylinder-axisymmetric** 


Not yet implemented.




Scripts:
- [`demo.py`](demo/convergence-tests/pulsating-cylinder-axisymmetric/demo.py)




#  Convergence test for "pulsating tube" 


directory name: **convergence-tests/pulsating-cylinder-cartesian** 


This code is an extenssion of [pulsating-cylinder-cartesian](demo/pulsating-cylinder-cartesian)



**[See more details](demo/convergence-tests/pulsating-cylinder-cartesian).**

<img src="demo/convergence-tests/pulsating-cylinder-cartesian/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/pulsating-cylinder-cartesian/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/pulsating-cylinder-cartesian/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/pulsating-cylinder-cartesian/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/pulsating-cylinder-cartesian/demo.py)
- [`screenshots.py`](demo/convergence-tests/pulsating-cylinder-cartesian/screenshots.py)


[pvsm paraview file](demo/convergence-tests/pulsating-cylinder-cartesian/results/vis.pvsm)



#  Illustrate different options to define regions with specific acoustic properties 


directory name: **define-regions-AutoSubDomain** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: using AutoSubDomain
- well defined materials interface



**[See more details](demo/define-regions-AutoSubDomain).**

<img src="demo/define-regions-AutoSubDomain/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-AutoSubDomain/demo.py)
- [`screenshots.py`](demo/define-regions-AutoSubDomain/screenshots.py)


[pvsm paraview file](demo/define-regions-AutoSubDomain/results/vis.pvsm)



#  Illustrate different options to define regions with specific acoustic properties 


directory name: **define-regions-SubDomain** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: creating a subclass of `SubDomain`
- not so well defined materials interface



**[See more details](demo/define-regions-SubDomain).**

<img src="demo/define-regions-SubDomain/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-SubDomain/demo.py)
- [`screenshots.py`](demo/define-regions-SubDomain/screenshots.py)


[pvsm paraview file](demo/define-regions-SubDomain/results/vis.pvsm)



#  Illustrate different options to define regions with specific acoustic properties 


directory name: **define-regions-set_subdomain** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: using `set_subdomain`
- well defined materials interface



**[See more details](demo/define-regions-set_subdomain).**

<img src="demo/define-regions-set_subdomain/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-set_subdomain/demo.py)
- [`screenshots.py`](demo/define-regions-set_subdomain/screenshots.py)


[pvsm paraview file](demo/define-regions-set_subdomain/results/vis.pvsm)



#  Illustrate different options to define regions with specific acoustic properties 


directory name: **define-regions-simple** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: simple
- not so well defined materials interface



**[See more details](demo/define-regions-simple).**

<img src="demo/define-regions-simple/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-simple/demo.py)
- [`screenshots.py`](demo/define-regions-simple/screenshots.py)


[pvsm paraview file](demo/define-regions-simple/results/vis.pvsm)



#  Diffraction from a corner 


directory name: **diffraction-corner** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency


<img src="demo/diffraction-corner/results/screenshot_1.png" height="120">
<img src="demo/diffraction-corner/results/screenshot_2.png" height="120">
<img src="demo/diffraction-corner/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/diffraction-corner/demo.py)
- [`screenshots.py`](demo/diffraction-corner/screenshots.py)


[pvsm paraview file](demo/diffraction-corner/results/vis.pvsm)



#  Diffraction from a rounded corner 


directory name: **diffraction-rounded-corner** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency
- sweep for the radius of the rounded corner
- local mesh refinement


<img src="demo/diffraction-rounded-corner/results/screenshot_1.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_2.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_3.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_4.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/diffraction-rounded-corner/demo.py)
- [`screenshots.py`](demo/diffraction-rounded-corner/screenshots.py)


[pvsm paraview file](demo/diffraction-rounded-corner/results/vis.pvsm)



#  Dipole in free space 2D 


directory name: **dipole** 


- 2D free space geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- radiation


<img src="demo/dipole/results/screenshot_1.png" height="120">
<img src="demo/dipole/results/screenshot_2.png" height="120">
<img src="demo/dipole/results/screenshot_3.png" height="120">
<img src="demo/dipole/results/screenshot_4.png" height="120">
<img src="demo/dipole/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/dipole/demo.py)
- [`screenshots.py`](demo/dipole/screenshots.py)


[pvsm paraview file](demo/dipole/results/vis.pvsm)



# `duct-hr-traveling`




<img src="demo/duct-hr-traveling/results/screenshot_1.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_2.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_3.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/duct-hr-traveling/demo.py)
- [`screenshots.py`](demo/duct-hr-traveling/screenshots.py)


[pvsm paraview file](demo/duct-hr-traveling/results/vis.pvsm)

Developers execution time: 0m48.255s


# `duct-irises-traveling`




<img src="demo/duct-irises-traveling/results/screenshot_1.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_2.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_3.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/duct-irises-traveling/demo.py)
- [`screenshots.py`](demo/duct-irises-traveling/screenshots.py)


[pvsm paraview file](demo/duct-irises-traveling/results/vis.pvsm)

Developers execution time: 0m36.438s


#  Helmholtz resonator (axisymmetric) 


directory name: **helmholtz-resonator-axisymmetric** 


- cylinder geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- wall velocity boundary condition
- frequency sweep
- resonance
- radiation


<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-axisymmetric/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-axisymmetric/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-axisymmetric/results/vis.pvsm)



#  Helmholtz resonator (external excitation) 


directory name: **helmholtz-resonator-external-excitation** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- Dirichlet boundary condition
- frequency sweep
- resonance
- radiation



**[See more details](demo/helmholtz-resonator-external-excitation).**

<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-external-excitation/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-external-excitation/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-external-excitation/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-external-excitation/results/vis.pvsm)



#  Helmholtz resonator (point source) 


directory name: **helmholtz-resonator-point-source** 


- **not working**
- 2D rectangular geometry
- anechoic walls
- point source
- absorbing region
- overriding `k_expression`
- frequency sweep
- resonance
- radiation



**[See more details](demo/helmholtz-resonator-point-source).**

<img src="demo/helmholtz-resonator-point-source/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-point-source/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-point-source/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-point-source/results/vis.pvsm)



#  Helmholtz resonator (vibrating wall) 


directory name: **helmholtz-resonator-vibrating-wall** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- wall velocity boundary condition
- frequency sweep
- resonance
- radiation


<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-vibrating-wall/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-vibrating-wall/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-vibrating-wall/results/vis.pvsm)



#  Horn 


directory name: **horn** 


- cylinder geometry
- axisymmetic
- resonance
- nomal modes
- frequency sweep
- absorbing region
- anechoic walls
- overriding `k_expression`
- wall velocity boundary condition
- radiation



**[See more details](demo/horn).**

<img src="demo/horn/results/screenshot_1.png" height="120">
<img src="demo/horn/results/screenshot_2.png" height="120">
<img src="demo/horn/results/screenshot_3.png" height="120">
<img src="demo/horn/results/screenshot_4.png" height="120">
<img src="demo/horn/results/screenshot_5a.png" height="120">
<img src="demo/horn/results/screenshot_5b.png" height="120">
<img src="demo/horn/results/screenshot_6a.png" height="120">
<img src="demo/horn/results/screenshot_6b.png" height="120">


Scripts:
- [`demo.py`](demo/horn/demo.py)
- [`screenshots.py`](demo/horn/screenshots.py)


[pvsm paraview file](demo/horn/results/vis.pvsm)



#  Modes in 2D rectangular duct 


directory name: **modes-rectangular-duct** 


- 2D infinite duct geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep



**[See more details](demo/modes-rectangular-duct).**

<img src="demo/modes-rectangular-duct/results/screenshot_1.png" height="120">
<img src="demo/modes-rectangular-duct/results/screenshot_2.png" height="120">
<img src="demo/modes-rectangular-duct/results/screenshot_3.png" height="120">
<img src="demo/modes-rectangular-duct/results/screenshot_4.png" height="120">
<img src="demo/modes-rectangular-duct/results/screenshot_5.png" height="120">
<img src="demo/modes-rectangular-duct/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/modes-rectangular-duct/demo.py)
- [`screenshots.py`](demo/modes-rectangular-duct/screenshots.py)


[pvsm paraview file](demo/modes-rectangular-duct/results/vis.pvsm)



#  Normal modes in simple axisymmetric domain 


directory name: **normal-modes-axisymmetric-simple** 


- Petri dish geometry
- **modified speed of sound**
- point source
- reflecting walls
- normal modes
- resonance
- axisymmetic
- frequency sweep


<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_1.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-axisymmetric-simple/demo.py)
- [`screenshots.py`](demo/normal-modes-axisymmetric-simple/screenshots.py)


[pvsm paraview file](demo/normal-modes-axisymmetric-simple/results/vis.pvsm)



#  Normal modes in simple axisymmetric domain (comparison with analytical solution) 


directory name: **normal-modes-axisymmetric-simple-analytic-comparison** 


- Petri dish geometry
- **modified speed of sound**
- point source
- reflecting walls
- normal modes
- resonance
- axisymmetic
- analytic solution: Bessel functions (for specific frequencies)
- frequency sweep


<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_1a.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_1b.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_3.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-axisymmetric-simple-analytic-comparison/demo.py)
- [`screenshots.py`](demo/normal-modes-axisymmetric-simple-analytic-comparison/screenshots.py)


[pvsm paraview file](demo/normal-modes-axisymmetric-simple-analytic-comparison/results/vis.pvsm)



#  Normal modes in 2D rectangular domain 


directory name: **normal-modes-rectangular-room** 


- 2D shoebox geometry
- point source
- reflecting walls
- resonance
- nomal modes
- frequency sweep


<img src="demo/normal-modes-rectangular-room/results/screenshot_1.png" height="120">
<img src="demo/normal-modes-rectangular-room/results/screenshot_2.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-rectangular-room/demo.py)
- [`screenshots.py`](demo/normal-modes-rectangular-room/screenshots.py)


[pvsm paraview file](demo/normal-modes-rectangular-room/results/vis.pvsm)



#  Normal modes in 2D rectangular domain (comparison with analytical solution) 


directory name: **normal-modes-rectangular-room-analytic-comparison** 


- 2D shoebox geometry
- point source
- reflecting walls
- resonance
- nomal modes
- analytic solution: cosine functions (for specific frequencies)
- frequency sweep
- results normalization



**[See more details](demo/normal-modes-rectangular-room-analytic-comparison).**

<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_1a.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_1b.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-rectangular-room-analytic-comparison/demo.py)
- [`screenshots.py`](demo/normal-modes-rectangular-room-analytic-comparison/screenshots.py)


[pvsm paraview file](demo/normal-modes-rectangular-room-analytic-comparison/results/vis.pvsm)



#  Beam in oblique incidence to another media 


directory name: **oblique-incidence** 


- 2D rectangular geometry
- oblique incidence
- overriding `k_expression`
- reflection
- refraction
- anechoic walls
- absorbing region
- gaussian beam
- microphone (receiving transducer)
- fixed frequency
- angle sweep
- ultrasound
- multiple media
- local mesh refinement



**[See more details](demo/oblique-incidence).**

<img src="demo/oblique-incidence/results/screenshot_1.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_2.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_3.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_4.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/oblique-incidence/demo.py)
- [`screenshots.py`](demo/oblique-incidence/screenshots.py)


[pvsm paraview file](demo/oblique-incidence/results/vis.pvsm)



#  Plane wave propagation 


directory name: **plane-wave** 


- 2D freespace geometry
- oblique propagation
- sinusoidal plane wave
- Dirichlet boundary condition
- analytic solution: sinusoidal plane wave
- frequency sweep


<img src="demo/plane-wave/results/screenshot_1.png" height="120">
<img src="demo/plane-wave/results/screenshot_2.png" height="120">
<img src="demo/plane-wave/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/plane-wave/demo.py)
- [`screenshots.py`](demo/plane-wave/screenshots.py)


[pvsm paraview file](demo/plane-wave/results/vis.pvsm)



#  Point source in free space 2D 


directory name: **point-source-2D** 


- 2D free space geometry
- anechoic boundary condition
- point source
- overriding `k_expression`
- frequency sweep
- radiation


<img src="demo/point-source-2D/results/screenshot_1.png" height="120">
<img src="demo/point-source-2D/results/screenshot_2.png" height="120">
<img src="demo/point-source-2D/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/point-source-2D/demo.py)
- [`screenshots.py`](demo/point-source-2D/screenshots.py)


[pvsm paraview file](demo/point-source-2D/results/vis.pvsm)



#  Point source in free space 3D  


directory name: **point-source-3D** 


- 3D free space geometry
- anechoic boundary condition
- fixed frequency
- point source
- overriding `k_expression`
- local mesh refinement
- radiation
- results normalization



**[See more details](demo/point-source-3D).**

<img src="demo/point-source-3D/results/screenshot_1.png" height="120">
<img src="demo/point-source-3D/results/screenshot_2.png" height="120">
<img src="demo/point-source-3D/results/screenshot_3.png" height="120">
<img src="demo/point-source-3D/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/point-source-3D/demo.py)
- [`screenshots.py`](demo/point-source-3D/screenshots.py)


[pvsm paraview file](demo/point-source-3D/results/vis.pvsm)



#  Pulsating cylinder in free space 


directory name: **pulsating-cylinder-cartesian** 


- 2D freespace geometry
- pulsating circle (cylinder)
- wall velocity boundary condition
- anechoic walls
- radiation
- frequency sweep
- analytic solution: (TODO) Check a sign (see code)



**[See more details](demo/pulsating-cylinder-cartesian).**

<img src="demo/pulsating-cylinder-cartesian/results/screenshot_1.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_2.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_3.png" height="120">
<img src="demo/pulsating-cylinder-cartesian/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-cylinder-cartesian/demo.py)
- [`screenshots.py`](demo/pulsating-cylinder-cartesian/screenshots.py)


[pvsm paraview file](demo/pulsating-cylinder-cartesian/results/vis.pvsm)



#  Pulsating sphere (using gmsh) 


directory name: **pulsating-sphere-gmsh** 


- 3D free space geometry
- single octant
- usign gmsh (mesh refinement)
- anechoic boundary condition
- fixed frequency
- radiation
- overriding `k_expression`
- analytic solution: (TODO: implement) pulsating sphere



**[See more details](demo/pulsating-sphere-gmsh).**

<img src="demo/pulsating-sphere-gmsh/results/screenshot_1.png" height="120">
<img src="demo/pulsating-sphere-gmsh/results/screenshot_2.png" height="120">
<img src="demo/pulsating-sphere-gmsh/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-sphere-gmsh/demo.py)
- [`screenshots.py`](demo/pulsating-sphere-gmsh/screenshots.py)


[pvsm paraview file](demo/pulsating-sphere-gmsh/results/vis.pvsm)



#  Pulsating sphere (first approach) 


directory name: **pulsating-sphere-simple** 


- **not working**
- 3D free space geometry
- anechoic boundary condition
- fixed frequency
- pulsating sphere
- overriding `k_expression`
- local mesh refinement
- radiation



**[See more details](demo/pulsating-sphere-simple).**

<img src="demo/pulsating-sphere-simple/results/screenshot_1.png" height="120">
<img src="demo/pulsating-sphere-simple/results/screenshot_2.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-sphere-simple/demo.py)
- [`screenshots.py`](demo/pulsating-sphere-simple/screenshots.py)


[pvsm paraview file](demo/pulsating-sphere-simple/results/vis.pvsm)



#  Pulsating sphere (single octant) 


directory name: **pulsating-sphere-single-octant** 


- **not working**
- 3D free space geometry
- single octant
- anechoic boundary condition
- fixed frequency
- pulsating sphere
- overriding `k_expression`
- local mesh refinement
- radiation



**[See more details](demo/pulsating-sphere-single-octant).**

<img src="demo/pulsating-sphere-single-octant/results/screenshot_1.png" height="120">
<img src="demo/pulsating-sphere-single-octant/results/screenshot_2.png" height="120">
<img src="demo/pulsating-sphere-single-octant/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-sphere-single-octant/demo.py)
- [`screenshots.py`](demo/pulsating-sphere-single-octant/screenshots.py)


[pvsm paraview file](demo/pulsating-sphere-single-octant/results/vis.pvsm)



#  Illustrate admittance boundary condition (`AutoSubDomains`) 


directory name: **shoebox-3D-absorbing-patch-AutoSubDomain** 


- **not yet implemented**
- 3D shoebox geometry
- mesh generated with gmsh
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep
- define regions: using `AutoSubDomains`



**[See more details](demo/shoebox-3D-absorbing-patch-AutoSubDomain).**



Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-AutoSubDomain/demo.py)




#  Illustrate admittance boundary condition (gmsh) 


directory name: **shoebox-3D-absorbing-patch-gmsh** 


- **not yet implemented**
- 3D shoebox geometry
- mesh generated with gmsh
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep



**[See more details](demo/shoebox-3D-absorbing-patch-gmsh).**



Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-gmsh/demo.py)




#  Illustrate admittance boundary condition (`set_subdomains`) 


directory name: **shoebox-3D-absorbing-patch-set_subdomain** 


- **not working**
- 3D shoebox geometry
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep



**[See more details](demo/shoebox-3D-absorbing-patch-set_subdomain).**



Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-set_subdomain/demo.py)




#  Illustrate admittance boundary condition (first approach) 


directory name: **shoebox-3D-absorbing-patch-simple** 


- 3D shoebox geometry
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep



**[See more details](demo/shoebox-3D-absorbing-patch-simple).**

<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_1.png" height="120">
<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_2.png" height="120">
<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-simple/demo.py)
- [`screenshots.py`](demo/shoebox-3D-absorbing-patch-simple/screenshots.py)


[pvsm paraview file](demo/shoebox-3D-absorbing-patch-simple/results/vis.pvsm)



#  Transmission over an irregular hole between two rooms 


directory name: **transmission-hole-two-rooms** 


- 2D irregular geometry 
- wall velocity boundary condition
- transmission
- frequency sweep
- resonance
- normal modes



**[See more details](demo/transmission-hole-two-rooms).**

<img src="demo/transmission-hole-two-rooms/results/screenshot_1.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_2a.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_2b.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_3.png" height="120">
<img src="demo/transmission-hole-two-rooms/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/transmission-hole-two-rooms/demo.py)
- [`screenshots.py`](demo/transmission-hole-two-rooms/screenshots.py)


[pvsm paraview file](demo/transmission-hole-two-rooms/results/vis.pvsm)



#  Propagation of two plane waves  


directory name: **two-plane-waves** 


- 2D freespace geometry
- oblique propagation
- sinusoidal plane wave
- Dirichlet boundary condition
- analytic solution: sinusoidal plane wave
- angle sweep
- fixed frequency
- **no simulation**



**[See more details](demo/two-plane-waves).**

<img src="demo/two-plane-waves/results/screenshot_1.png" height="120">
<img src="demo/two-plane-waves/results/screenshot_2.png" height="120">
<img src="demo/two-plane-waves/results/screenshot_3.png" height="120">
<img src="demo/two-plane-waves/results/screenshot_4.png" height="120">
<img src="demo/two-plane-waves/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/two-plane-waves/demo.py)
- [`screenshots.py`](demo/two-plane-waves/screenshots.py)


[pvsm paraview file](demo/two-plane-waves/results/vis.pvsm)



#  2D Check for the velocity field 


directory name: **velocity_check_2D** 


- 2D shoebox geometry
- wall admittance boundary condition
- reflecting walls
- Dirichlet boundary condition
- frequency sweep
- velocity field
- microphones



**[See more details](demo/velocity_check_2D).**

<img src="demo/velocity_check_2D/results/screenshot_2.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_3.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_4.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_5.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_6.png" height="120">
<img src="demo/velocity_check_2D/results/screenshot_7.png" height="120">


Scripts:
- [`demo_A.py`](demo/velocity_check_2D/demo_A.py)
- [`demo_B.py`](demo/velocity_check_2D/demo_B.py)
- [`screenshots.py`](demo/velocity_check_2D/screenshots.py)


[pvsm paraview file](demo/velocity_check_2D/results/vis.pvsm)

Developers execution time: 0m18.895s


