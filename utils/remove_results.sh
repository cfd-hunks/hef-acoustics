#!/bin/bash
#

for item in $(find demo -type d -name results)
do
    echo "Removing results from $item"

    # Thanks to https://stackoverflow.com/questions/1885525
    read -r -p "Are you sure? [Y/n]" response
    response=${response,,} # tolower
    if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
        find "$item" -name "*.vtu" -delete -print
        find "$item" -name "*.pvd" -delete -print
    fi
done
